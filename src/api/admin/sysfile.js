import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/admin/sysfile/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/admin/sysfile',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/admin/sysfile/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/admin/sysfile/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/admin/sysfile',
    method: 'put',
    data: obj
  })
}

export function getOssConfig() {
  return request({
    url: '/admin/sysfile/ossConfig',
    method: 'get'
  })
}

export function updateOssConfig(obj) {
  return request({
    url: '/admin/sysfile/saveConfig',
    method: 'post',
    data: obj
  })
}
