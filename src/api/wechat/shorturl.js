import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/shorturl/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/shorturl',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/cac/shorturl/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/cac/shorturl/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/cac/shorturl',
    method: 'put',
    data: obj
  })
}
