import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/gamectrlcfg/page',
    method: 'get',
    params: query
  })
}

export function doFqzsCfg(obj) {
  return request({
    url: '/cac/gamectrlcfg/doFqzsCfg',
    method: 'post',
    data: obj
  })
}

export function getFqzsCfg(query) {
  return request({
    url: '/cac/gamectrlcfg/info',
    method: 'get',
    params: query
  })
}
