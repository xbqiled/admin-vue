import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/report/maintenance/game/page',
    method: 'get',
    params: query
  })
}

export function doMaint(query) {
  return request({
    url: '/report/maintenance/game/maint',
    method: 'get',
    params: query
  })
}
