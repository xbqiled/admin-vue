import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/gamectrlcfgstr/page',
    method: 'get',
    params: query
  })
}

export function doCfg(obj) {
  return request({
    url: '/cac/gamectrlcfgstr/doCfg',
    method: 'post',
    data: obj
  })
}

export function getCfg(keyId) {
  return request({
    url: '/cac/gamectrlcfgstr/info/' + keyId,
    method: 'get'
  })
}

export function getGameList() {
  return request({
    url: '/cac/gamectrlcfgstr/getGameList/',
    method: 'get'
  })
}
