import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/xinboonoff/all',
    method: 'get',
    params: query
  })
}

export function updateCfg(obj) {
  return request({
    url: '/cac/xinboonoff',
    method: 'post',
    data: obj
  })
}
