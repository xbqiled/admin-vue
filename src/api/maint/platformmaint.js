import request from '@/router/axios'

export function fetchList() {
  return request({
    url: '/report/maintenance/platform/page',
    method: 'get'
  })
}

export function doMaint(query) {
  return request({
    url: '/report/maintenance/platform/maint',
    method: 'get',
    params: query
  })
}
