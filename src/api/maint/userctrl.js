import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/userctrl/page',
    method: 'get',
    params: query
  })
}

export function addConfig(obj) {
  return request({
    url: '/cac/userctrl/doSet',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/cac/userctrl/' + id,
    method: 'get'
  })
}


export function getRoom() {
  return request({
    url: '/cac/userctrl/roomList',
    method: 'get'
  })
}


export function delObj(id) {
  return request({
    url: '/cac/userctrl/cancelCtrl',
    method: 'get'
  })
}

export function cancelCtrl(obj) {
  return request({
    url: '/cac/userctrl/cancelCtrl',
    method: 'get',
    params: obj
  })
}
