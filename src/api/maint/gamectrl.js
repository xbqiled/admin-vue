import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/gamectrl/page',
    method: 'get',
    params: query
  })
}

export function doSet(obj) {
  return request({
    url: '/cac/gamectrl/doSet',
    method: 'post',
    data: obj
  })
}


export function getRoomTree() {
  return request({
    url: '/cac/gamectrl/getRoomTree',
    method: 'post'
  })
}
