import request from '@/router/axios'


export function doAction(query) {
  return request({
    url: '/report/maintenance/gmaction',
    method: 'get',
    params: query
  })
}
