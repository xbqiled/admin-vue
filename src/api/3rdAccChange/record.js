import request from '@/router/axios'
export function query3rdAccChangeRecord(query) {
  return request({
    url: '/report/3rd/acc_change_records',
    method: 'get',
    params: query
  })
}
