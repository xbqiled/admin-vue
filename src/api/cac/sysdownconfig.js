import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/downconfig/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/downconfig',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/cac/downconfig/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/cac/downconfig/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/cac/downconfig',
    method: 'put',
    data: obj
  })
}


export function activate(id, channelId) {
  return request({
    url: '/cac/downconfig/activate/id=' + id + '&channelId=' + channelId,
    method: 'get'
  })
}
