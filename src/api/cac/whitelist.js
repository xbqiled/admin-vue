import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/whitelist/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/whitelist',
    method: 'post',
    params: obj
  })
}

export function delObj(id) {
  return request({
    url: '/cac/whitelist/' + id,
    method: 'delete'
  })
}

