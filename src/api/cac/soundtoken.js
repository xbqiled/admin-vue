import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/soundtoken/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/soundtoken',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/cac/soundtoken/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/cac/soundtoken/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/cac/soundtoken',
    method: 'put',
    data: obj
  })
}
