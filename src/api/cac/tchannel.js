import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/tchannel/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/tchannel',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/cac/tchannel/' + id,
    method: 'get'
  })
}

export function getConfigObj(id) {
  return request({
    url: '/cac/tchannel/config/' + id,
    method: 'get'
  })
}


export function getGameConfigObj(id) {
  return request({
    url: '/cac/tchannel/gameConfig/' + id,
    method: 'get'
  })
}

export function doGameConfig(query) {
  return request({
    url: '/cac/tchannel/doGameConfig',
    method: 'post',
    params: query
  })
}

export function addConfigObj(query) {
  return request({
    url: '/cac/tchannel/saveConfig',
    method: 'get',
    params: query
  })
}

export function delObj(id) {
  return request({
    url: '/cac/tchannel/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/cac/tchannel',
    method: 'put',
    data: obj
  })
}
