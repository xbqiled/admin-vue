import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/enterpriseconfig/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/enterpriseconfig',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/cac/enterpriseconfig/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/cac/enterpriseconfig/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/generator/wxenterpriseconfig',
    method: 'put',
    data: obj
  })
}
