import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/pushconfig/page',
    method: 'get',
    params: query
  })
}

export function getPushPlatformData() {
  return request({
    url: '/cac/pushplatform/getPushPlatformData',
    method: 'get'
  })
}

export function doconfig(obj) {
  return request({
    url: '/cac/pushconfig/doconfig/',
    method: 'post',
    data: obj
  })
}

export function activate(id) {
  return request({
    url: '/cac/pushconfig/activate/' + id,
    method: 'get'
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/pushconfig/add',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/cac/pushconfig/getObj/' + id,
    method: 'get'
  })
}

export function getConfigById(id) {
  return request({
    url: '/cac/pushconfig/getConfigById/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/cac/pushconfig/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/cac/pushconfig/update',
    method: 'post',
    data: obj
  })
}
