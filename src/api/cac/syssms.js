import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/syssms/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/syssms',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/cac/syssms/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/cac/syssms/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/cac/syssms',
    method: 'put',
    data: obj
  })
}

export function sendSms() {
  return request({
    url: '/cac/syssms/sendSms',
    method: 'get'
  })
}
