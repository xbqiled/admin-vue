import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/pushrecord/page',
    method: 'get',
    params: query
  })
}

export function sendPush(obj) {
  return request({
    url: '/cac/pushrecord/sendPush',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/cac/pushrecord/getById/' + id,
    method: 'get'
  })
}

export function getReportById(id) {
  return request({
    url: '/cac/pushrecord/getReportById/' + id,
    method: 'get'
  })
}
