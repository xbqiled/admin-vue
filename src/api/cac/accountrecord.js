import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/accountrecord/page',
    method: 'get',
    params: query
  })
}
