import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/accountinfo/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/accountinfo',
    method: 'post',
    data: obj
  })
}

export function getObj(tAccountid) {
  return request({
    url: '/cac/accountinfo/' + tAccountid,
    method: 'get'
  })
}

export function accountObj(id) {
  return request({
    url: '/cac/accountinfo/getUserInfo/' + id,
    method: 'get'
  })
}

export function activateState(query) {
  return request({
    url: '/cac/accountinfo/activate',
    method: 'get',
    params: query
  })
}

export function modifyPw(query) {
  return request({
    url: '/cac/accountinfo/modifyPw',
    method: 'get',
    params: query
  })
}

export function resetPw(query) {
  return request({
    url: '/cac/accountinfo/resetPw',
    method: 'get',
    params: query
  })
}

export function kickUser(id) {
  return request({
    url: '/cac/accountinfo/kickUser/' + id,
    method: 'get'
  })
}

export function clearCommonDev(id) {
  return request({
    url: '/cac/accountinfo/clearCommonDev/' + id,
    method: 'get'
  })
}

export function unbindPhoneNum(id) {
  return request({
    url: '/cac/accountinfo/unbindPhoneNum/' + id,
    method: 'get'
  })
}

export function isInGame(id) {
  return request({
    url: '/cac/accountinfo/isOnline/' + id,
    method: 'get'
  })
}

export function modifyBankPw(query) {
  return request({
    url: '/cac/accountinfo/modifyBankPw',
    method: 'get',
    params: query
  })
}


export function resetBankPw(query) {
  return request({
    url: '/cac/accountinfo/resetBankPw',
    method: 'get',
    params: query
  })
}


export function delObj(id) {
  return request({
    url: '/cac/accountinfo/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/cac/accountinfo',
    method: 'put',
    data: obj
  })
}
