import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/downloadpanelcfg/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/downloadpanelcfg',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/cac/downloadpanelcfg/getConfig/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/cac/downloadpanelcfg/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/cac/downloadpanelcfg',
    method: 'put',
    data: obj
  })
}
