import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/chllog/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/chllog',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/cac/chllog/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/cac/chllog/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/cac/chllog',
    method: 'put',
    data: obj
  })
}
