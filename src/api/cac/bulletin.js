import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/bulletin/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/bulletin',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/cac/bulletin/' + id,
    method: 'get'
  })
}

export function viewChannel(id) {
  return request({
    url: '/cac/bulletin/getChannel/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/cac/bulletin/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/cac/bulletin',
    method: 'put',
    data: obj
  })
}
