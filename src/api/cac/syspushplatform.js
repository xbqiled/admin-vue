import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/pushplatform/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/pushplatform',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/cac/pushplatform/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/cac/pushplatform/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/cac/pushplatform',
    method: 'put',
    data: obj
  })
}
