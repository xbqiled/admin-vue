import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/channelaccount/page',
    method: 'get',
    params: query
  })
}

export function putObj(obj) {
  return request({
    url: '/cac/channelaccount',
    method: 'put',
    data: obj
  })
}
