import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/usermail/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/usermail',
    method: 'post',
    data: obj
  })
}

export function getObj(obj) {
  return request({
    url: '/cac/usermail/findById',
    method: 'get',
    params: obj
  })
}

export function delObj(id) {
  return request({
    url: '/cac/usermail/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/cac/usermail',
    method: 'put',
    data: obj
  })
}
