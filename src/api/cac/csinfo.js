import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/csinfo/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/csinfo',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/cac/csinfo/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/cac/csinfo/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/cac/csinfo',
    method: 'put',
    data: obj
  })
}
