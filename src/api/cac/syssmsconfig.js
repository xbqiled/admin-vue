import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/syssmsconfig/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/syssmsconfig',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/cac/syssmsconfig/' + id,
    method: 'get'
  })
}

export function activate(id) {
  return request({
    url: '/cac/syssmsconfig/activate/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/cac/syssmsconfig/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/cac/syssmsconfig',
    method: 'put',
    data: obj
  })
}

export function getSmsSetting(id) {
  return request({
    url: '/cac/syssmsconfig/setting/' + id,
    method: 'get',
  })
}

export function updateSmsSetting(id, obj) {
  return request({
    url: '/cac/syssmsconfig/saveSetting/' + id,
    method: 'post',
    data: obj
  })
}
