import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/downloadpanelcfg1/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/downloadpanelcfg1',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/cac/downloadpanelcfg1/getConfig/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/cac/downloadpanelcfg1/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/cac/downloadpanelcfg1',
    method: 'put',
    data: obj
  })
}
