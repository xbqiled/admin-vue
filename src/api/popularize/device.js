import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/deviceinfo/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/deviceinfo',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/cac/deviceinfo/' + id,
    method: 'get'
  })
}

export function getDeviceInfo(id) {
  return request({
    url: '/cac/deviceinfo/getDeviceInfo/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/cac/deviceinfo/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/cac/deviceinfo',
    method: 'put',
    data: obj
  })
}
