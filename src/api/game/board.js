import request from '@/router/axios'

export function byfetchList(query) {
  return request({
    url: '/report/statisreport/byboard/page',
    method: 'get',
    params: query
  })
}

export function ddzfetchList(query) {
  return request({
    url: '/report/statisreport/ddzboard/page',
    method: 'get',
    params: query
  })
}

export function lhfetchList(query) {
  return request({
    url: '/report/statisreport/lhboard/page',
    method: 'get',
    params: query
  })
}

export function nnfetchList(query) {
  return request({
    url: '/report/statisreport/nnboard/page',
    method: 'get',
    params: query
  })
}

export function bjlfetchList(query) {
  return request({
    url: '/report/statisreport/bjlboard/page',
    method: 'get',
    params: query
  })
}

export function sgjfetchList(query) {
  return request({
    url: '/report/statisreport/sgjboard/page',
    method: 'get',
    params: query
  })
}

export function twentyOnefetchList(query) {
  return request({
    url: '/report/statisreport/twentyOneboard/page',
    method: 'get',
    params: query
  })
}

export function pdkfetchList(query) {
  return request({
    url: '/report/statisreport/pdkboard/page',
    method: 'get',
    params: query
  })
}

export function csdfetchList(query) {
  return request({
    url: '/report/statisreport/csdboard/page',
    method: 'get',
    params: query
  })
}

export function getCsdDetail(id) {
  return request({
    url: '/report/statisreport/csdboard/boardDetail/' + id,
    method: 'get'
  })
}

export function dtfetchList(query) {
  return request({
    url: '/report/statisreport/bhsboard/page',
    method: 'get',
    params: query
  })
}

export function zjhfetchList(query) {
  return request({
    url: '/report/statisreport/zjhboard/page',
    method: 'get',
    params: query
  })
}

export function hhdzfetchList(query) {
  return request({
    url: '/report/statisreport/hhdzboard/page',
    method: 'get',
    params: query
  })
}

export function getHhdzDetail(id) {
  return request({
    url: '/report/statisreport/hhdzboard/boardDetail/' + id,
    method: 'get'
  })
}


export function hbjlfetchList(query) {
  return request({
    url: '/report/statisreport/hbjlboard/page',
    method: 'get',
    params: query
  })
}

export function hhdzpoolList(query) {
  return request({
    url: '/report/redblackpool/page',
    method: 'get',
    params: query
  })
}


export function qznnfetchList(query) {
  return request({
    url: '/report/statisreport/qznnboard/page',
    method: 'get',
    params: query
  })
}


export function getQznnDetail(query) {
  return request({
    url: '/report/statisreport/qznnboard/boardDetail',
    method: 'get',
    params: query
  })
}

export function fqzsfetchList(query) {
  return request({
    url: '/report/statisreport/fqzsboard/page',
    method: 'get',
    params: query
  })
}

export function getFqzsDetail(query) {
  return request({
    url: '/report/statisreport/fqzsboard/boardDetail',
    method: 'get',
    params: query
  })
}

export function bcbmfetchList(query) {
  return request({
    url: '/report/statisreport/bcbmboard/page',
    method: 'get',
    params: query
  })
}

export function getBcbmDetail(query) {
  return request({
    url: '/report/statisreport/bcbmboard/boardDetail',
    method: 'get',
    params: query
  })
}
