import request from '@/router/axios'

export function queryLottery(query) {
  return request({
    url: '/report/3rd/betting_records/lotteries',
    method: 'get',
    params: query
  })
}

export function queryCrownSport(query) {
  return request({
    url: '/report/3rd/betting_records/hgsport',
    method: 'get',
    params: query
  })
}

export function queryEGame(query) {
  return request({
    url: '/report/3rd/betting_records/egame',
    method: 'get',
    params: query
  })
}

export function queryLive(query) {
  return request({
    url: '/report/3rd/betting_records/live',
    method: 'get',
    params: query
  })
}

export function query3rdSport(query) {
  return request({
    url: '/report/3rd/betting_records/3rdsport',
    method: 'get',
    params: query
  })
}
