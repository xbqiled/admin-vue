import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/cashorder/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/cashorder',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/cac/cashorder/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/cac/cashorder/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/cac/cashorder',
    method: 'put',
    data: obj
  })
}


export function doApproval(query) {
  return request({
    url: '/cac/cashorder/approval',
    method: 'get',
    params: query
  })
}
