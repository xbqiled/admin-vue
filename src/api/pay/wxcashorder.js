import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/wxcashorder/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/wxcashorder',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/cac/wxcashorder/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/cac/wxcashorder/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/generator/wxcashorder',
    method: 'put',
    data: obj
  })
}
