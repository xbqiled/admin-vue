import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/payorder/page',
    method: 'get',
    params: query
  })
}


export function rechargeOrder(query) {
  return request({
    url: '/cac/payorder/recharge',
    method: 'get',
    params: query
  })
}

export function reduceOrder(query) {
  return request({
    url: '/cac/payorder/reduce',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/payorder',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/cac/payorder/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/cac/payorder/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/cac/payorder',
    method: 'put',
    data: obj
  })
}
