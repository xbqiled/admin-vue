import request from '@/router/axios'

export function statisGameList(query) {
  return request({
    url: '/cac/statisgame/gameList',
    method: 'get',
    params: query
  })
}

export function statisUserGameList(query) {
  return request({
    url: '/cac/statisgame/usergameList',
    method: 'post',
    params: query
  })
}

export function gamelist() {
  return request({
    url: '/cac/statisgame/allGame',
    method: 'get'
  })
}
