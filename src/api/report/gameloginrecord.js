import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/report/logingameRecord/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/report/logingameRecord',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/report/logingameRecord/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/report/logingameRecord/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/report/logingameRecord',
    method: 'put',
    data: obj
  })
}
