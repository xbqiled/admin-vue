import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/report/rechargeplatformorder/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/report/rechargeplatformorder',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/report/rechargeplatformorder/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/report/rechargeplatformorder/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/report/rechargeplatformorder',
    method: 'put',
    data: obj
  })
}
