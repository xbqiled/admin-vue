import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/report/dantiaosingle/page',
    method: 'get',
    params: query
  })
}
