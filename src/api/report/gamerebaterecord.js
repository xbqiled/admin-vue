import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/report/gamerebate/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/report/gamerebate',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/report/gamerebate/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/report/gamerebate/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/report/vrecordgamerebatedetail',
    method: 'put',
    data: obj
  })
}
