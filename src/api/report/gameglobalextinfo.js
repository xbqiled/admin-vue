import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/gameglobalextinfo/page',
    method: 'get',
    params: query
  })
}
