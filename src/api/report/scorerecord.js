import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/report/scoreRecord/page',
    method: 'get',
    params: query
  })
}
