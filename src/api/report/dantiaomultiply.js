import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/report/dantiaomultiply/page',
    method: 'get',
    params: query
  })
}
