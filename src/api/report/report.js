import request from '@/router/axios'

export function dayfetchList(query) {
  return request({
    url: '/cac/statischannel/dayReport/page',
    method: 'get',
    params: query
  })
}

export function weekfetchList(query) {
  return request({
    url: '/report/statisreport/week/page',
    method: 'get',
    params: query
  })
}

export function monthfetchList(query) {
  return request({
    url: '/cac/statischannel/monthReport/page',
    method: 'get',
    params: query
  })
}

export function wealthfetchList(query) {
  return request({
    url: '/report/statisreport/wealth/page',
    method: 'get',
    params: query
  })
}

