import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/report/platformonlinecount/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/report/platformonlinecount',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/report/platformonlinecount/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/report/platformonlinecount/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/report/platformonlinecount',
    method: 'put',
    data: obj
  })
}
