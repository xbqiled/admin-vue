import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/report/treasureChange/page',
    method: 'get',
    params: query
  })
}
