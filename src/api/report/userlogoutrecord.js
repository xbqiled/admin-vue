import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/report/userlogoutRecord/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/report/userlogoutRecord',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/report/userlogoutRecord/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/report/userlogoutRecord/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/report/userlogoutRecord',
    method: 'put',
    data: obj
  })
}
