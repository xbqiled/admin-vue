import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/statisaccount/statisList',
    method: 'get',
    params: query
  })
}
