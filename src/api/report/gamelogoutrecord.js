import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/report/logoutgameRecord/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/report/logoutgameRecord',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/report/logoutgameRecord/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/report/logoutgameRecord/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/report/logoutgamerecord',
    method: 'put',
    data: obj
  })
}
