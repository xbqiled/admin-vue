import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/invitedeposit/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/invitedeposit/doConfig',
    method: 'post',
    data: obj
  })
}

export function getObj(queryParam) {
  return request({
    url: '/cac/invitedeposit/info',
    method: 'get',
    params: queryParam
  })
}

export function delObj(id) {
  return request({
    url: '/cac/invitedeposit/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/cac/invitedeposit',
    method: 'put',
    data: obj
  })
}
