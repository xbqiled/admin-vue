import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/nickAward/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/nickAward/doConfig',
    method: 'post',
    data: obj
  })
}

export function getObj(queryParam) {
  return request({
    url: '/cac/nickAward/info/'+queryParam.channelId,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/cac/nickAward/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/cac/nickAward',
    method: 'put',
    data: obj
  })
}
