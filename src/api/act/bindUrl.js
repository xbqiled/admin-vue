import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/bindUrl/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/bindUrl/doConfig',
    method: 'post',
    data: obj
  })
}

export function delObj(obj) {
  return request({
    url: '/cac/bindUrl/delete',
    method: 'post',
    data: obj
  })
}
