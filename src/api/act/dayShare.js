import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/dayShare/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/dayShare/doConfig',
    method: 'post',
    data: obj
  })
}

export function getObj(channelId) {
  return request({
    url: '/cac/dayShare/info/'+channelId,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/cac/dayShare/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/cac/dayShare',
    method: 'put',
    data: obj
  })
}
