import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/deposit/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/deposit/doConfig',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/cac/deposit/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/cac/deposit/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/cac/deposit',
    method: 'put',
    data: obj
  })
}
