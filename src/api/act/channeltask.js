import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/channeltask/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/channeltask/doConfig',
    method: 'post',
    data: obj
  })
}

export function getObj(channelId) {
  return request({
    url: '/cac/channeltask/info/'+channelId,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/cac/channeltask/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/cac/channeltask',
    method: 'put',
    data: obj
  })
}
