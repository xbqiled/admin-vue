import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/channelsuccour/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/channelsuccour/doConfig',
    method: 'post',
    data: obj
  })
}

export function getObj(channelId) {
  return request({
    url: '/cac/channelsuccour/info/'+channelId,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/cac/channelsuccour/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/cac/channelsuccour',
    method: 'put',
    data: obj
  })
}
