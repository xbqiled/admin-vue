import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/bind/page',
    method: 'get',
    params: query
  })
}

export function doset(obj) {
  return request({
    url: '/cac/bind/doSet',
    method: 'post',
    data: obj
  })
}

export function getObj(channelId) {
  return request({
    url: '/cac/bind/' + channelId,
    method: 'get'
  })
}
