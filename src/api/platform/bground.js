import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/bground/page',
    method: 'get',
    params: query
  })
}
