import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/bguser/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/bguser',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/cac/bguser/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/cac/bguser/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/cac/bguser',
    method: 'put',
    data: obj
  })
}

export function changeStatus(status) {
  return request({
    url: '/cac/bguser/modifyUserStatus',
    method: 'post',
    data: status
  })
}
