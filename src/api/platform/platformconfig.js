import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/platformconfig/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/platformconfig',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/cac/platformconfig/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/cac/platformconfig/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/cac/platformconfig',
    method: 'put',
    data: obj
  })
}

export function getPlatformConfig(id) {
  return request({
    url: '/cac/platformconfig/getPlatformConfig/' + id ,
    method: 'get'
  })
}

export function updatePlatformConfig(obj) {
  return request({
    url: '/cac/platformconfig/doConfig',
    method: 'post',
    data: obj
  })
}
