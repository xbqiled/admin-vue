import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/bgenterconfig/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/bgenterconfig',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/cac/bgenterconfig/getConfig/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/cac/bgenterconfig/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/cac/bgenterconfig',
    method: 'put',
    data: obj
  })
}
