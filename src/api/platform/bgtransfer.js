import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/cac/bgtransfer/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/cac/bgtransfer',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/cac/bgtransfer/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/cac/bgtransfer/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/cac/bgtransfer',
    method: 'put',
    data: obj
  })
}
