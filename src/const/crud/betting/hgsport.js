export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menu: false,
  dic: [],
  column: [
    {
      label: '赛事编号 / 单号 / 投注时间',
      prop: 'orderNum',
      solt: true,
      width: 200
    },
    {
      label: '用户ID',
      prop: 'userId',
      width: 100
    },
    {
      label: '用户昵称',
      prop: 'userName',
      width: 140
    },
    {
      label: '投注类型',
      prop: 'bettingType',
      solt: true,
      width: 155
    },
    {
      label: '赛事ID / 投注项',
      prop: 'gid',
      solt: true,
      width: 320
    },
    {
      label: '投注金额',
      prop: 'bettingMoney',
      width: 100,
      align: 'right'
    },
    {
      label: '提交状态',
      prop: "bettingStatus",
      width: 100,
      solt: true,
      dicData: [{
        label: '待确认',
        value: 1
      }, {
        label: '已确认',
        value: 2
      }, {
        label: '系统取消',
        value: 3
      },{
        label: "手动取消",
        value: 4
      }]
    }, {
      label: '派彩金额',
      prop: 'bettingResult',
      width: 100,
      solt: true,
      align: 'right'
    },{
      label: '亏损金额',
      prop: 'ks',
      width: 100,
      solt: true,
      align: 'right'
    }, {
      label: '结算状态',
      prop: 'balance',
      width: 100,
      solt: true,
      dicData: [{
        label: '未结算',
        value: 1
      }, {
        label: '已结算',
        value: 2
      }]
    }, {
      label: '渠道编码',
      prop: 'channelId'
    }
  ]
}
