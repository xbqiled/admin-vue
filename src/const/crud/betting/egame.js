export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menu: false,
  dic: [],
  column: [
    {
      label: '电子游艺游戏类型',
      prop: 'type',
      width: 130,
      dicData: [{
        label: 'AG电子游戏',
        value: 1
      }, {
        label: 'BBIN电子游戏',
        value: 2
      }, {
        label: 'MG电子游戏',
        value: 3
      }, {
        label: "QT电子游戏",
        value: 4
      }, {
        label: "PT电子游戏",
        value: 6
      }, {
        label: "CQ9电子游戏",
        value: 9
      }, {
        label: "JDB电子游戏",
        value: 11
      }, {
        label: "EBET电子游戏",
        value: 13
      }]
    },
    {
      label: '用户ID',
      prop: 'userId',
      width: 90
    },
    {
      label: '用户昵称',
      prop: 'userName',
      width: 130
    }, {
      label: '注单号',
      prop: 'bettingCode',
      width: 280
    }, {
      label: '局号',
      prop: 'tableCode',
      width: 120
    }, {
      label: '游戏名称',
      prop: 'gameType',
      width: 100
    }, {
      label: '投注金额',
      prop: 'bettingMoney',
      width: 80,
      align: 'right'
    }, {
      label: '有效投注金额',
      prop: 'realBettingMoney',
      width: 100,
      align: 'right'
    }, {
      label: '中奖金额',
      prop: 'winMoney',
      width: 80,
      align: 'right'
    },{
      label: '亏损金额',
      prop: 'ks',
      width: 80,
      solt: true,
      align: 'right'
    },{
      label: '投注时间',
      prop: 'bettingTime',
      width: 135
    },{
      label: '创建时间',
      prop: 'createDatetime',
      width: 135
    },
    {
      label: '渠道编码',
      prop: 'channelId'
    }
  ]
}
