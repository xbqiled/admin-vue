export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menu: false,
  dic: [],
  column: [
    {
      label: '类型',
      prop: 'type',
      width: 80,
      dicData: [{
        label: 'AG',
        value: 1
      }, {
        label: 'BBIN',
        value: 2
      }, {
        label: 'MG',
        value: 3
      }, {
        label: "ALLBET",
        value: 5
      }, {
        label: "PT",
        value: 6
      }, {
        label: "OG",
        value: 7
      }, {
        label: "DS",
        value: 8
      }, {
        label: "EBET",
        value: 13
      }, {
        label: "BG",
        value: 98
      }]
    },
    {
      label: '用户ID',
      prop: 'userId',
      width: 90
    },
    {
      label: '用户昵称',
      prop: 'userName',
      width: 130
    }, {
      label: '注单号',
      prop: 'bettingCode',
      width: 260
    }, {
      label: '局号',
      prop: 'gameCode',
      width: 120
    }, {
      label: '投注内容',
      prop: 'bettingContent',
      width: 100
    }, {
      label: '游戏名称',
      prop: 'gameType',
      width: 100
    }, {
      label: '投注金额',
      prop: 'bettingMoney',
      width: 80,
      align: 'right'
    }, {
      label: '有效投注金额',
      prop: 'realBettingMoney',
      width: 100,
      align: 'right'
    }, {
      label: '中奖金额',
      prop: 'winMoney',
      width: 80,
      align: 'right'
    }, {
      label: '亏损金额',
      prop: 'ks',
      width: 80,
      solt: true,
      align: 'right'
    }, {
      label: '投注时间',
      prop: 'bettingTime',
      width: 135
    }, {
      label: '创建时间',
      prop: 'createDatetime',
      width: 135
    },
    {
      label: '渠道编码',
      prop: 'channelId'
    }
  ]
}
