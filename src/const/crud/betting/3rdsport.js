export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menu: false,
  dic: [],
  column: [
    {
      label: '类型',
      prop: 'type',
      width: 80,
      dicData: [ {
        label: 'BBIN',
        value: 2
      }, {
        label: "IBC",
        value: 10
      }, {
        label: "M8",
        value: 99
      }, {
        label: "M8H",
        value: 991
      }]
    },
    {
      label: '用户ID',
      prop: 'userId',
      width: 90
    },
    {
      label: '用户昵称',
      prop: 'userName',
      width: 130
    }, {
      label: '注单号',
      prop: 'bettingCode',
      width: 120
    }, {
      label: '体育 / 投注类型',
      prop: 'sportType',
      solt: true,
      width: 120
    }, {
      label: '联赛名称 / 队伍 / 赔率',
      prop: 'league',
      solt: true,
      width: 250
    }, {
      label: '注单结果',
      prop: 'resStatus',
      width: 100,
      dicData: [ {
        label: '未结算',
        value: 1
      }, {
        label: "全赢",
        value: 2
      }, {
        label: "全输",
        value: 3
      }, {
        label: "赢一半",
        value: 4
      }, {
        label: "输一半",
        value: 5
      }, {
        label: "和局",
        value: 6
      }, {
        label: "已结算",
        value: 9
      }, {
        label: "取消注单",
        value: 10
      }]
    }, {
      label: '投注金额',
      prop: 'bettingMoney',
      width: 80,
      align: 'right'
    }, {
      label: '有效投注金额',
      prop: 'realBettingMoney',
      width: 100,
      align: 'right'
    }, {
      label: '中奖金额',
      prop: 'winMoney',
      width: 80,
      align: 'right'
    }, {
      label: '亏损金额',
      prop: 'ks',
      width: 80,
      solt: true,
      align: 'right'
    }, {
      label: '投注时间',
      prop: 'bettingTime',
      width: 135
    }, {
      label: '创建时间',
      prop: 'createDatetime',
      width: 135
    },
    {
      label: '渠道编码',
      prop: 'channelId'
    }
  ]
}
