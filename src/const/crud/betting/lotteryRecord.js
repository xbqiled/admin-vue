export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menu: false,
  dic: [],
  column: [
    {
      label: '注单号',
      prop: 'orderId',
      solt: true,
      width: 120
    },
    {
      label: '用户昵称',
      prop: 'userName',
      width: 100
    },
    {
      label: '用户ID',
      prop: 'userId',
      width: 100
    },
    {
      label: '彩种名称',
      prop: 'lotName',
      width: 100
    },
    {
      label: '期号',
      prop: 'qiHao',
      width: 115
    },
    {
      label: '玩法名称',
      prop: 'playName',
      width: 80
    },
    {
      label: '投注号码',
      prop: 'haoMa',
      width: 120
    },
    {
      label: '投注时间',
      prop: 'createTime',
      width: 155
    },
    {
      label: '注数',
      prop: 'buyZhuShu',
      width: 55
    },
    {
      label: '倍数',
      prop: 'multiple',
      width: 55
    },
    {
      label: '模式',
      prop: 'model',
      width: 50,
      dicData: [{
        label: '元',
        value: 1
      }, {
        label: '角',
        value: 10
      }, {
        label: '分',
        value: 100
      }]
    },
    {
      label: '投注金额',
      prop: 'buyMoney',
      width: 80,
      align: 'right'
    },
    {
      label: '中奖金额',
      prop: 'winMoney',
      width: 80,
      align: 'right'
    },
    {
      label: '投注IP',
      prop: 'betIp',
      width: 120
    },
    {
      label: '会员备注',
      prop: 'remark'
    },
    {
      label: '状态',
      prop: 'status',
      solt: true,
      span: 24,
      dicData: [{
        label: '未开奖',
        value: 1
      }, {
        label: '已中奖',
        value: 2
      }, {
        label: '未中奖',
        value: 3
      }, {
        label: '撤单',
        value: 4
      }, {
        label: '派奖回滚成功',
        value: 5
      }, {
        label: '回滚异常',
        value: 6
      }, {
        label: '开奖异常',
        value: 7
      }]
    },
    {
      label: '渠道编码',
      prop: 'channelId',
      width: 110
    }
  ]
}
