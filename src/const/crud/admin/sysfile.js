export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
	  {
      label: 'ID',
      prop: 'id'
    },
	  {
      label: '名称',
      prop: 'name'
    },
	  {
      label: '全名',
      prop: 'fileName'
    },
	  {
      label: '后缀名',
      prop: 'suffix'
    },
	  {
      label: '类型',
      prop: 'type'
    },
    {
      label:'图片',
      prop:'fileUrl',
      type:'upload',
      imgWidth: 50,
      imgHeight: 50,
      listType:'picture-img'
    },
	  {
      label: '数据',
      prop: 'fileData',
      hide: true,
      editDisabled: false,
      addVisdiplay: false
    },
	  {
      label: '创建时间',
      prop: 'createTime',
      align: 'center',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm',
      valueFormat: 'yyyy-MM-dd hh:mm:ss',
      editDisabled: true,
      addVisdiplay: false
    },
  ]
}
