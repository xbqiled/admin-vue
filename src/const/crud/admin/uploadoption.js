export default {
  labelWidth: 0,
  menuBtn: false,
  submitBtn: false,
  align: 'center',
  column: [{
    name:'file',
    label: '',
    prop: 'imgUrl',
    type: 'upload',
    span: 24,
    drag: true,
    tip: '只能上传jpg/png文件，且不超过1000kb',
    action: '/admin/sysfile/upload/',
  }]
}
