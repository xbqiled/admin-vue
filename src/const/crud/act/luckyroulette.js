export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
	  {
      label: '渠道ID',
      prop: 'channelId',
      search: true
    },
	  {
      label: '渠道名称',
      prop: 'channelName'
    },
    {
      label: '图标',
      prop:'iconUrl',
      type:'upload',
      imgWidth: 50,
      imgHeight: 50,
      listType:'picture-img'
    },
	  {
      label: '白银消耗积分',
      prop: 'silverCost'
    },
	  {
      label: '黄金消耗积分',
      prop: 'goldCost'
    },
	  {
      label: '钻石消耗积分',
      prop: 'diamondCost'
    },
	  {
      label: '活动开始时间',
      prop: 'startTime'
    },
	  {
      label: '活动结束时间',
      prop: 'finishTime'
    }
  ]
}
