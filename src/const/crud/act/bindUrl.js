export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
	  {
      label: '渠道ID',
      prop: 'channelId',
      search: true
    },
	  {
      label: '渠道名称',
      prop: 'channelName'
    },
	  {
      label: '图标',
      prop:'iconUrl',
      type:'upload',
      imgWidth: 50,
      imgHeight: 50,
      listType:'picture-img'
    },
    {
      label: '域名',
      prop: 'url'
    },
    {
      label: '绑定上级ID',
      prop: 'accountId'
    },
  ]
}
