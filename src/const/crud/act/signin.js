export const tableOption = {
  border: true,
  index: false,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
	  {
      label: '渠道ID',
      prop: 'channelId'
    },
	  {
      label: '渠道名称',
      prop: 'channelName'
    },
	  {
      label: '图标',
      prop:'iconUrl',
      type:'upload',
      imgWidth: 50,
      imgHeight: 50,
      listType:'picture-img'
    },
	  {
      label: '开始时间',
      prop: 'startTimeStr'
    },
	  {
      label: '结束时间',
      prop: 'endTimeStr'
    },
    {
      label: '是否循环',
      prop: 'isCirculate',
      type: 'select',
      dicData:
        [{
          label: '是',
          value: true
        }, {
          label: '否',
          value: false
        }]
     },
    {
      label: '状态',
      prop: 'openType',
      type: 'select',
      dicData:
        [{
          label: '正常',
          value: true
        }, {
          label: '失效',
          value: false
        }]
    }
  ]
}
