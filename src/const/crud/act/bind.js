export const tableOption = {
  border: true,
  index: false,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  menuWidth: 100,
  delBtn: false,
  addBtn: false,
  menu: true,
  dic: [],
  column: [
    {
      width: 100,
      label: '渠道图标',
      prop:'iconUrl',
      type:'upload',
      imgWidth: 50,
      imgHeight: 50,
      listType:'picture-img'
    },
	  {
      width: 100,
      label: '渠道编码',
      prop: 'channelId',
      search: true,
    },
    {
      width: 150,
      label: '渠道名称',
      prop: 'channelName'
    },
	  {
      width: 150,
      label: '转正赠送金额',
      prop: 'giveValue'
    },
    {
      width: 150,
      label: '第一级返利',
      prop: 'rebate'
    },
    {
      width: 150,
      label: '是否开放兑换',
      prop: 'exchange',
      type: 'select',
      dicData:
        [{
          label: '关闭',
          value: 0
        }, {
          label: '开启',
          value: 1
        }]
    },
    {
      width: 150,
      label: '最低兑换额度',
      prop: 'minExchange'
    },
    {
      width: 150,
      label: '注册初始化金币',
      prop: 'initCoin'
    },
    {
      width: 150,
      label: '打码量倍率',
      prop: 'damaMulti'
    },
    {
      width: 150,
      label: '返利打码量倍率',
      prop: 'rebateDamaMulti'
    },
    {
      width: 150,
      label: '代理模式',
      prop: 'promoteType',
      type: 'select',
      dicData:
        [{
          label: '无限代理模式',
          value: 1
        }, {
          label: '单级代理模式',
          value: 2
        }]
    },
    {
      width: 150,
      label: '每日提款次数',
      prop: 'exchangeCount'
    },
    {
      width: 150,
      label: '重置打码量',
      prop: 'resetDamaLeftCoin'
    },
    {
      width: 150,
      label: '绑定彩票站点',
      prop: 'cpStationId'
    }
  ]
}
