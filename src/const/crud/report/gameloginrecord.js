export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menu: false,
  dic: [],
  column: [
	  {
      label: '用户ID',
      prop: 'UserID',
    },
    {
      label: '用户名',
      prop: 'userName',
    },
    {
      label: '用户昵称',
      prop: 'nickName',
    },
	  {
      label: '进入时金币',
      prop: 'BeforeScoreCount',
    },
	  {
      label: '游戏类型ID',
      prop: 'GameTypeID',
    },
    {
      label: '游戏名称',
      prop: 'gameKindName',
    },
    {
      label: '游戏房间',
      prop: 'phoneGameName',
    },
	  {
      label: '终端',
      prop: 'TerminalType',
      width: 150,
      type: 'select',
      dicData:
        [{
          label: 'PC端',
          value: 0
        }, {
          label: '安卓端',
          value: 1
        }, {
          label: '苹果端',
          value: 2
        }, {
          label: 'H5',
          value: 3
        }]
    },
	  {
      label: '进入时间',
      prop: 'RecordTime',
      width: 180,
    },
	  {
      label: '登陆关联标识',
      prop: 'MatchGuid',
      hide: true,
      width: 220,
    },
	  {
      label: '游戏关联标识',
      prop: 'KeyGuid',
      hide: true,
      width: 220,
    },
	  {
      label: '渠道编码',
      prop: 'ChannelNO',
      width: 150,
    }
  ]
}
