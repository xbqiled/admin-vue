export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menu: false,
  dic: [],
  column: [
	  {
      label: '用户ID',
      prop: 'userId'
    },
    {
      label: '用户名称',
      prop: 'accountName'
    },
    {
      label: '用户昵称',
      prop: 'nickName'
    },
    {
      label: '渠道号',
      prop: 'channelNO'
    },
	  {
      label: '用户类型',
      prop: 'userType',
      width: 150,
      hide: true,
      type: 'select',
      dicData: [{
        label: '真人',
        value: 2
      }, {
        label: '机器人',
        value: 1
      }]
    },
	  {
      label: '登陆平台',
      prop: 'loginType',
      type: 'select',
      hide: true,
      width: 150,
      dicData: [{
        label: '平台登陆',
        value: 0
      }, {
        label: '微信登陆',
        value: 1
      }, {
        label: 'SDK登陆',
        value: 2
      }]
    },
	  {
      label: '登陆IP',
      prop: 'loginIP'
    },
	  {
      label: '机器码',
      prop: 'machineSerial',
      hide:true,
      width: 150
    },
	  {
      label: '网络标识',
      prop: 'networdState',
      type: 'select',
      dicData: [{
        label: 'WIFI',
        value: 0
      }, {
        label: '2G',
        value: 1
      }, {
        label: '3G',
        value: 2
      }, {
        label: '4G',
        value: 3
      }]
    },
	  {
      label: 'ssid名称',
      prop: 'wifiName',
      hide: true,
      width: 120
    },
	  {
      label: '版本号',
      prop: 'version'
    },
	  {
      label: '客户端',
      prop: 'terminalType',
      type: 'select',
      dicData: [{
        label: 'PC端',
        value: 0
      }, {
        label: '安卓端',
        value: 1
      }, {
        label: '苹果端',
        value: 2
      }, {
        label: 'H5',
        value: 3
      }]
    },
	  {
      label: '机型',
      prop: 'machineType'
    },
	  {
      label: '操作系统',
      prop: 'systemType'
    },
	  {
      label: '关联标识',
      prop: 'matchGuid',
      hide:true,
      width: 180
    },
	  {
      label: '登录模式',
      prop: 'loginMode',
      type: 'select',
      hide: true,
      width: 120,
      dicData: [{
        label: '正常登录',
        value: 0
      }, {
        label: '重连',
        value: 1
      }]
    },
    {
      label: '登录时间',
      prop: 'loginTime',
      align: 'center',
      type: 'datetime',
      format: 'yyyy-MM-dd hh:mm:ss',
      valueFormat: 'timestamp',
      width: 180,
      editDisabled: true,
      addVisdiplay: false
    }
  ]
}
