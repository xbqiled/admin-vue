export const tableOption = {
  border: true,
  index: false,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menu: false,
  dic: [],
  column: [
	  {
      label: '渠道编码',
      prop: 'channelId'
    },
	  {
      label: '游戏ID',
      prop: 'gameId'
    },
	  {
      label: '游戏名称',
      prop: 'gameKindName'
    },
	  {
      label: '统计日期',
      prop: 'statisMonthTime'
    },
	  {
      label: '游戏次数',
      prop: 'gameCount'
    },
	  {
      label: '用户盈利(元)',
      prop: 'gameFee'
    },
	  {
      label: '下注金额(元)',
      prop: 'betFee'
    },
	  {
      label: '税收金额(元)',
      prop: 'revenueFee'
    },
	  {
      label: '是否比赛',
      prop: 'isMatch',
      type: 'select',
      dicData:
        [{
          label: '否',
          value: 1
        }, {
          label: '是',
          value: 2
        }]
    },
	  {
      label: '游戏时长(秒)',
      prop: 'gameTimeLength'
    }
  ]
}
