export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menu: false,
  dic: [],
  column: [
    {
      label: '渠道编码',
      prop: 'channelId',
      width: 150,
      valueDefault: 0
    },
    {
      label: '统计日期',
      prop: 'statisTime',
      width: 150
    },
	  {
      label: '充值金额',
      prop: 'rechargeFee',
      width: 150,
      valueDefault: 0,
      formatter: (row) => {
        var value = row['rechargeFee']
        if (value == null || value == undefined || value == '' || value == null) {
          return '0'
        }
        return value
      }
    },
	  {
      label: '充值人数',
      prop: 'rechargeUserCount',
      width: 150,
      valueDefault: 0,
      formatter: (row) => {
        var value = row['rechargeUserCount']
        if (value == null || value == undefined || value == '' || value == null) {
          return '0'
        }
        return value
      }
    },
    {
      label: '充值笔数',
      prop: 'rechargeCount',
      width: 150,
      valueDefault: 0,
      formatter: (row) => {
        var value = row['rechargeCount']
        if (value == null || value == undefined || value == '' || value == null) {
          return '0'
        }
        return value
      }
    },
    {
      label: '提现金额',
      prop: 'cashFee',
      width: 150,
      valueDefault: 0,
      formatter: (row) => {
        var value = row['cashFee']
        if (value == null || value == undefined || value == '' || value == null) {
          return '0'
        }
        return value
      }
    },
	  {
      label: '提现人数',
      prop: 'cashUserCount',
      width: 150,
      valueDefault: 0,
      formatter: (row) => {
        var value = row['cashUserCount']
        if (value == null || value == undefined || value == '' || value == null) {
          return '0'
        }
        return value
      }
    },
	  {
      label: '提现笔数 ',
      prop: 'cashCount',
      width: 150,
      valueDefault: 0,
      formatter: (row) => {
        var value = row['cashCount']
        if (value == null || value == undefined || value == '' || value == null) {
          return '0'
        }
        return value
      }
    },
	  {
      label: '当日盈亏',
      prop: 'loss',
      width: 150,
      valueDefault: 0,
      formatter: (row) => {
        var value = row['loss']
        if (value == null || value == undefined || value == '' || value == null) {
          return '0'
        }
        return value
      }
    },
	  {
      label: '注册人数',
      prop: 'regUserCount',
      width: 150,
      valueDefault: 0,
      formatter: (row) => {
        var value = row['regUserCount']
        if (value == null || value == undefined || value == '' || value == null) {
          return '0'
        }
        return value
      }
    },
    {
      label: '新增游戏人数',
      prop: 'newGameUserCount',
      width: 150,
      valueDefault: 0,
      formatter: (row) => {
        var value = row['newGameUserCount']
        if (value == null || value == undefined || value == '' || value == null) {
          return '0'
        }
        return value
      }
    },
	  {
      label: '游戏次数',
      prop: 'gameCount',
      width: 150,
      valueDefault: 0,
      formatter: (row) => {
        var value = row['gameCount']
        if (value == null || value == undefined || value == '' || value == null) {
          return '0'
        }
        return value
      }
    },
	  {
      label: '游戏人数',
      prop: 'gameUserCount',
      width: 150,
      valueDefault: 0,
      formatter: (row) => {
        var value = row['gameUserCount']
        if (value == null || value == undefined || value == '' || value == null) {
          return '0'
        }
        return value
      }
    },
    {
      label: '登录次数',
      prop: 'loginCount',
      width: 150,
      valueDefault: 0,
      formatter: (row) => {
        var value = row['loginCount']
        if (value == null || value == undefined || value == '' || value == null) {
          return '0'
        }
        return value
      }
    },
    {
      label: '登录人数',
      prop: 'loginUserCount',
      width: 150,
      valueDefault: 0,
      formatter: (row) => {
        var value = row['loginUserCount']
        if (value == null || value == undefined || value == '' || value == null) {
          return '0'
        }
        return value
      }
    },
    {
      label: '活跃用户',
      prop: 'dailyActiveCount',
      width: 150,
      valueDefault: 0,
      formatter: (row) => {
        var value = row['dailyActiveCount']
        if (value == null || value == undefined || value == '' || value == null) {
          return '0'
        }
        return value
      }
    },
    {
      label: '首充金额',
      prop: 'firstRechargeFee',
      width: 150,
      valueDefault: 0,
      formatter: (row) => {
        var value = row['firstRechargeFee']
        if (value == null || value == undefined || value == '' || value == null) {
          return '0'
        }
        return value
      }
    },
    {
      label: '首充人数',
      prop: 'firstRechargeCount',
      width: 150,
      valueDefault: 0,
      formatter: (row) => {
        var value = row['firstRechargeCount']
        if (value == null || value == undefined || value == '' || value == null) {
          return '0'
        }
        return value
      }
    },
    {
      label: '游戏时长(秒)',
      prop: 'timeLength',
      width: 150,
      valueDefault: 0,
      formatter: (row) => {
        var value = row['timeLength']
        if (value == null || value == undefined || value == '' || value == null) {
          return '0'
        }
        return value
        }
      },
    {
      label: '充值率%',
      prop: 'rechargeRate',
      width: 150,
      valueDefault: 0,
      formatter: (row) => {
        var value = row['rechargeRate']
        if (value == null || value == undefined || value == '' || value == null) {
          return '0'
        }
        return value
      }
    }
  ]
}
