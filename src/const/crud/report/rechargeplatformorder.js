export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  viewBtn: false,
  menu: false,
  dic: [],
  column: [
	  {
      label: 'ID',
      prop: 'id'
    },
	  {
      label: '订单号',
      prop: 'orderno'
    },
	  {
      label: '用户ID',
      prop: 'userid',
    },
	  {
      label: '账号',
      prop: 'account'
    },
	  {
      label: '关联配置表',
      prop: 'paytype'
    },
	  {
      label: '支付金额',
      prop: 'payamount'
    },
	  {
      label: '状态',
      prop: 'paystatus',
      type: 'select',
      dicData: [{
        label: '待付款',
        value: 0
      }, {
        label: '支付成功',
        value: 1
      }, {
        label: '支付失败',
        value: 2
      }]
    },
	  {
      label: '充值时间',
      prop: 'recordtime',
      align: 'center',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm:ss',
      valueFormat: 'yyyy-MM-dd HH:mm:ss'
    },
  ]
}
