export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
	  {
      label: '用户编码',
      prop: 'userId'
    },
	  {
      label: '用户名',
      prop: 'userName'
    },
	  {
      label: '用户昵称',
      prop: 'nickName'
    },
	  {
      label: '渠道编码',
      prop: 'channelId'
    },
	  {
      label: '统计时间',
      prop: 'statisTime'
    },
	  {
      label: '在线充值金额',
      prop: 'onlineRechargeFee'
    },
	  {
      label: '手动充值金额',
      prop: 'handRechargeFee'
    },
	  {
      label: '提现金额',
      prop: 'withdrawFee'
    },
	  {
      label: '赠送,奖励,金额',
      prop: 'giveFee'
    },
	  {
      label: '打码量(下注金額)',
      prop: 'betFee'
    },
	  {
      label: '游戏金額',
      prop: 'statisFee'
    },
	  {
      label: '扣減金額',
      prop: 'reduceFee'
    },
  ]
}
