export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menu: false,
  dic: [],
  column: [
	  {
      label: '用户ID',
        prop: 'UserId',
    },
    {
      label: '用户名',
      prop: 'userName',
    },
    {
      label: '用户昵称',
      prop: 'nickName',
    },
	  {
      label: '用户余额',
      prop: 'GainCount'
    },
	  {
      label: '渠道编码',
      prop: 'channelNO',
    },
    {
      label: '渠道名称',
      prop: 'channelName',
    },
  ]
}
