export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menu: false,
  dic: [],
  column: [
	  {
      label: '牌局ID',
      prop: 'RecordID',
      width: 150
    },
    {
      label: '用户ID',
      prop: 'accountId',
      width: 150
    },
    {
      label: '之前是否已退出放水模式',
      prop: 'beforeIsExitOut',
      width: 150,
      type: 'select',
      dicData:
        [{
          label: '否',
          value: 0
        }, {
          label: '是',
          value: 1
        }]
    },
	  {
      label: '之前是否需要保护',
      prop: 'beforeIsProtect',
      width: 150,
      type: 'select',
      dicData:
        [{
          label: '否',
          value: 0
        }, {
          label: '是',
          value: 1
        }]
    },
	  {
      label: '之前签号',
      prop: 'beforeId',
      width: 150
    },
    {
      label: '之前放水模式累计输赢(分)',
      prop: 'beforeOutTotalWin',
      width: 150
    },
    {
      label: '之前需要保护总次数',
      prop: 'beforeNeedProtectTotalCnt',
      width: 150
    },
    {
      label: '之前已经保护次数',
      prop: 'beforeActualProtectCnt',
      width: 150
    },
    {
      label: '之后是否已退出放水模式',
      prop: 'afterIsExitOut',
      width: 150,
      type: 'select',
      dicData:
        [{
          label: '否',
          value: 0
        }, {
          label: '是',
          value: 1
        }]
    },
    {
      label: '之后是否需要保护',
      prop: 'afterIsProtect',
      width: 150,
      type: 'select',
      dicData:
        [{
          label: '否',
          value: 0
        }, {
          label: '是',
          value: 1
        }]
    },
    {
      label: '之后签号',
      prop: 'afterId',
      width: 150
    },
    {
      label: '之后放水模式累计输赢(分)',
      prop: 'afterOutTotalWin',
      width: 150
    },
    {
      label: '之后需要保护总次数',
      prop: 'afterNeedProtectTotalCnt',
      width: 150
    },
    {
      label: '之后已经保护次数',
      prop: 'afterActualProtectCnt',
      width: 150
    },
    {
      label: '之前总洗码量(分)',
      prop: 'beforeTotalXiMa',
      width: 150
    },
    {
      label: '之前总抽水金额(分)',
      prop: 'beforeTotalTax',
      width: 150
    },
    {
      label: '之前奖池余额(分)',
      prop: 'beforeRewardPoolCoin',
      width: 150
    },
    {
      label: '之前系统总盈亏(分)',
      prop: 'beforeTotalWinLose',
      width: 150
    },
    {
      label: '之后总洗码量(分)',
      prop: 'afterTotalXiMa',
      width: 150
    },
    {
      label: '之后总抽水金额(分)',
      prop: 'afterTotalTax',
      width: 150
    },
    {
      label: '之后奖池余额(分)',
      prop: 'afterRewardPoolCoin',
      width: 150
    },
    {
      label: '之后系统总盈亏(分)',
      prop: 'afterTotalWinLose',
      width: 150
    },
    {
      label: '之前是否在控制',
      prop: 'beforeIsNewDataCtl',
      width: 150,
      type: 'select',
      dicData:
        [{
          label: '否',
          value: 0
        }, {
          label: '是',
          value: 1
        }]
    },
    {
      label: '之前控制类型',
      prop: 'beforeNewDataCtlType',
      width: 150,
      type: 'select',
      dicData:
        [{
          label: '无',
          value: 0
        }, {
          label: '吃大赔小',
          value: 1
        }, {
          label: '吃小赔大',
          value: 2
        }]
    },
    {
      label: '之前需要控制总局数',
      prop: 'beforeNeedTotalCtrlInning',
      width: 150
    },
    {
      label: '之前已经控制局数',
      prop: 'beforeActualCtrlInning',
      width: 150
    },
    {
      label: '之前奖池余额最高可输(分)',
      prop: 'beforeRandPoolMaxLose',
      width: 150
    },
    {
      label: '之前奖池余额最高可赢(分)',
      prop: 'beforeRandPoolMaxWin',
      width: 150
    },
    {
      label: '之前最低局数(最高可输)',
      prop: 'beforeRandLoseBegin',
      width: 150
    },
    {
      label: '之前最高局数(最高可输)',
      prop: 'beforeRandLoseEnd',
      width: 150
    },
    {
      label: '之前最低局数(最高可赢)',
      prop: 'beforeRandWinBegin',
      width: 150
    },
    {
      label: '之前最高局数(最高可赢)',
      prop: 'beforeRandWinEnd',
      width: 150
    },
    {
      label: '之后是否在控制',
      prop: 'afterIsNewDataCtl',
      width: 150,
      type: 'select',
      dicData:
        [{
          label: '否',
          value: 0
        }, {
          label: '是',
          value: 1
        }]
    },
    {
      label: '之后控制类型',
      prop: 'afterNewDataCtlType',
      width: 150,
      type: 'select',
      dicData:
        [{
          label: '无',
          value: 0
        }, {
          label: '吃大赔小',
          value: 1
        }, {
          label: '吃小赔大',
          value: 2
        }]
    },
    {
      label: '之后需要控制总局数',
      prop: 'afterNeedTotalCtrlInning',
      width: 150
    },
    {
      label: '之后已经控制局数',
      prop: 'afterActualCtrlInning',
      width: 150
    },
    {
      label: '之后奖池余额最高可输(分)',
      prop: 'afterRandPoolMaxLose',
      width: 180
    },
    {
      label: '之后奖池余额最高可赢(分)',
      prop: 'afterRandPoolMaxWin',
      width: 180
    },
    {
      label: '之后最低局数(最高可输)',
      prop: 'afterRandLoseBegin',
      width: 180
    },
    {
      label: '之后最高局数(最高可输)',
      prop: 'afterRandLoseEnd',
      width: 180
    },
    {
      label: '之后最低局数(最高可赢)',
      prop: 'afterRandWinBegin',
      width: 180
    },
    {
      label: '之后最高局数(最高可赢)',
      prop: 'afterRandWinEnd',
      width: 180
    },
    {
      label: '总在线人数',
      prop: 'onlinePlayerCnt',
      width: 180
    },
    {
      label: '四种花色下注人数(不含王)',
      prop: 'betFourColorPlayerCnt',
      width: 180
    },
    {
      label: '本局洗码量(不含王)',
      prop: 'curTotalXima',
      width: 180
    },
    {
      label: '记录时间',
      prop: 'recordTime',
      width: 180
    },
    {
      label: '游戏房间',
      prop: 'gameKindName',
      width: 180
    }
  ]
}
