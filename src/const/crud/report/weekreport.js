export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menu: false,
  dic: [],
  column: [
    {
      label: '渠道编码',
      prop: 'channelId',
      width: 150,
      valueDefault: 0
    },
    {
      label: '统计日期',
      prop: 'recordTime',
      width: 150
    },
    {
      label: '注册人数',
      prop: 'regCount',
      width: 150,
      valueDefault: 0,
      formatter: (row) => {
        var value = row['regCount']
        if (value == null || value == undefined || value == '' || value == null) {
          return '0'
        }
        return value
      }
    },
    {
      label: '新增游戏人数',
      prop: 'newGameCount',
      width: 150,
      valueDefault: 0,
      formatter: (row) => {
        var value = row['newGameCount']
        if (value == null || value == undefined || value == '' || value == null) {
          return '0'
        }
        return value
      }
    },
    {
      label: '游戏次数',
      prop: 'loginUserCount',
      width: 150,
      valueDefault: 0,
      formatter: (row) => {
        var value = row['loginUserCount']
        if (value == null || value == undefined || value == '' || value == null) {
          return '0'
        }
        return value
      }
    },
    {
      label: '登录人数',
      prop: 'logincount',
      width: 100,
      valueDefault: 0,
      formatter: (row) => {
        var value = row['logincount']
        if (value == null || value == undefined || value == '' || value == null) {
          return '0'
        }
        return value
      }
    },
    {
      label: '用户游戏时长 ',
      prop: 'gameTimeCount',
      width: 150,
      valueDefault: 0,
      hide: true,
      formatter: (row) => {
        var value = row['gameTimeCount']
        if (value == null || value == undefined || value == '' || value == null) {
          return '0'
        }
        return value
      }
    },
    {
      label: '游戏平均时长',
      prop: 'newUserAvgGameTime',
      width: 150,
      valueDefault: 0,
      hide: true,
      formatter: (row) => {
        var value = row['newUserAvgGameTime']
        if (value == null || value == undefined || value == '' || value == null) {
          return '0'
        }
        return value
      }
    },
    {
      label: '充值人数',
      prop: 'rechargeCount',
      valueDefault: 0,
      formatter: (row) => {
        var value = row['rechargeCount']
        if (value == null || value == undefined || value == '' || value == null) {
          return '0'
        }
        return value
      }
    },
    {
      label: '充值总金额',
      prop: 'totalRechargeMoney',
      valueDefault: 0,
      formatter: (row) => {
        var value = row['totalRechargeMoney']
        if (value == null || value == undefined || value == '' || value == null) {
          return '0'
        }
        return value
      }
    },
    {
      label: 'ARPU',
      prop: 'arpu',
      valueDefault: 0,
      hide: true,
      formatter: (row) => {
        var value = row['arpu']
        if (value == null || value == undefined || value == '' || value == null) {
          return '0'
        }
        return value
      }
    },
    {
      label: '充值率',
      prop: 'rechargeRate',
      valueDefault: 0,
      formatter: (row) => {
        var value = row['rechargeRate']
        if (value == null || value == undefined || value == '' || value == null) {
          return '0'
        }
        return value
      }
    }
  ]
}
