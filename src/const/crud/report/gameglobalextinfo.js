export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
	  {
      label: '游戏id',
      prop: 'tGameatomtypeid'
    },
	  {
      label: '扩展信息',
      prop: 'tExtinfo'
    },
	  {
      label: '更新时间',
      prop: 'tUpdatetime'
    },
  ]
}
