export const tableOption = {
  border: true,
  index: false,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menu: false,
  dic: [],
  column: [
    {
      label: '订单号',
      prop: 'orderId'
    },
    {
      label: '用户ID',
      prop: 'accountId'
    },
    {
      label: '用户名',
      prop: 'userName'
    },
    {
      label: '用户昵称',
      prop: 'nickName'
    },
    {
      label: '充值金额(分)',
      prop: 'rechargeCoin'
    },
    {
      label: '返利金额(分)',
      prop: 'rebateCoin'
    },
    {
      label: '返利类型',
      prop: 'opType',
      type: 'select',
      dicData:
        [{
          label: '充值返利',
          value: 1
        }, {
          label: '扣除返利',
          value: 2
        }]
    },
    {
      label: '返利前金币',
      prop: 'beforeCoin'
    },
    {
      label: '金币变化',
      prop: 'changeCoin'
    },
    {
      label: '返利后金币',
      prop: 'afterCoin'
    },
    {
      label: '之后金币',
      prop: 'afterCoin'
    },
    {
      label: '返利时间',
      prop: 'recordTime'
    }
  ]
}
