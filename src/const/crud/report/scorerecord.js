export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menu: false,
  dic: [],
  column: [
	  {
      label: 'ID',
      prop: 'ID',
      hide: true
    },
	  {
      label: '用户ID',
      prop: 'UserID'
    },
	  {
      label: '(今日)之前积分',
      prop: 'LastBeforeCount'
    },
	  {
      label: '(今日)更新积分',
      prop: 'LastGainCount'
    },
	  {
      label: '(今日)之后积分',
      prop: 'LastAfterCount'
    },
    {
      label: '(明日)之前积分',
      prop: 'BeforeCount'
    },
    {
      label: '(明日)更新积分',
      prop: 'GainCount'
    },
    {
      label: '(明日)之后积分',
      prop: 'AfterCount'
    },
	  {
      label: '类型',
      prop: 'Type',
      type: 'select',
      width: 120,
      dicData: [{
        label: 'WIFI',
        value: 0
      }, {
        label: '0点清零',
        value: 1
      }, {
        label: '下注赠送',
        value: 2
      }, {
        label: '抽奖消耗',
        value: 3
      }, {
        label: 'BG下注',
        value: 4
      }]
    },
	  {
      label: '记录时间',
      prop: 'RecordTime'
    },
	  {
      label: '游戏名称',
      prop: 'gameKindName'
    },
	  {
      label: '游戏房间',
      prop: 'phoneGameName'
    }
  ]
}
