export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menu: false,
  dic: [],
  column: [
	  {
      label: '用户ID',
      prop: 'userId',
    },
    /*{
      label: '用户名称',
      prop: 'accountName'
    },*/
    {
      label: '用户昵称',
      prop: 'nickName'
    },
	  {
      label: '在线时长',
      prop: 'onLineDuration'
    },
	  {
      label: '退出时间',
      prop: 'logoutTime',
      align: 'center',
      type: 'datetime',
      format: 'yyyy-MM-dd hh:mm:ss',
      valueFormat: 'timestamp',
      editDisabled: true,
      addVisdiplay: false
    },
	  {
      label: '关联标识',
      prop: 'matchGuid'
    },
	  {
      label: '退出类型',
      prop: 'logoutType',
      type: 'select',
      dicData: [{
        label: '密码修改被踢',
        value: 1
      }, {
        label: '顶号',
        value: 2
      }, {
        label: '正常退出',
        value: 3
      }, {
        label: '掉线',
        value: 4
      }, {
        label: '重连',
        value: 5
      }]
    },
    {
      label: '渠道编码',
      prop: 'channelNO'
    }
  ]
}
