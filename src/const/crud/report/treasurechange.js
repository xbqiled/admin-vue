export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menu: false,
  dic: [],
  column: [
	  {
      label: 'ID',
      prop: 'ID',
      hide: true
    },
	  {
      label: '用户ID',
      prop: 'UserID',
      width: 100
    },
	  {
      label: '类型',
      prop: 'UserType',
      type: 'select',
      width: 120,
      dicData:
        [{
          label: '机器人',
          value: 1
        }, {
          label: '真人',
          value: 2
        }]
    },
	  {
      label: '道具ID',
      prop: 'ItemID',
      hide: true
    },
	  {
      label: '之前数量',
      prop: 'BeforeCount'
    },
	  {
      label: '更新数量',
      prop: 'GainCount'
    },
	  {
      label: '之后数量',
      prop: 'AfterCount'
    },
	  {
      label: '业务类型',
      prop: 'comment',
      width: 300
    },
	  {
      label: '游戏名称',
      prop: 'gameKindName',
      width: 150,
    },
    {
      label: '房间ID',
      prop: 'GameTypeId',
      width: 100,
    },
    {
      label: '游戏房间',
      prop: 'phoneGameName',
      width: 250,
    },
	  {
      label: '记录时间',
      prop: 'RecordTime',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm:ss',
      valueFormat: 'timestamp',
      editDisabled: true,
      addVisdiplay: false,
      width: 200,
    },
	  {
      label: '道具增加有效时间',
      prop: 'ItemValidValue',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm',
      valueFormat: 'timestamp',
      hide: true
    },
	  {
      label: '之前有效时间',
      prop: 'BeforeValidValue',
      hide: true
    },
	  {
      label: '之后有效时间',
      prop: 'AfterValidValue',
      hide: true
    },
	  {
      label: '登录关联标识',
      prop: 'MatchGuid',
      hide: true
    },
	  {
      label: '游戏关联标识',
      prop: 'KeyGuid',
      hide: true
    },
	  {
      label: '业务关联标识',
      prop: 'BusinessGuid',
      hide: true
    },
	  {
      label: '操作端',
      prop: 'TerminalType',
      type: 'select',
      width: 120,
      dicData:
        [{
          label: 'PC端',
          value: 0
        }, {
          label: '安卓端',
          value: 1
        }, {
          label: '苹果端',
          value: 2
        }, {
          label: 'H5',
          value: 3
        }]
    },
    {
      label: '渠道编码',
      prop: 'ChannelNO',
      width: 120
    },
  ]
}
