export const tableOption = {
  border: true,
  index: false,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menu: false,
  dic: [],
  column: [
    {
      label: '渠道编码',
      prop: 'channel'
    },
    {
      label: '用户ID',
      prop: 'accountId'
    },
    {
      label: '用户名',
      prop: 'userName'
    },
    {
      label: '用户昵称',
      prop: 'nickName'
    },
    {
      label: '第几天签到',
      prop: 'dayIndex'
    },
    {
      label: '今日下注量',
      prop: 'todayBet'
    },
    {
      label: '天数奖励',
      prop: 'award'
    },
    {
      label: '额外奖励',
      prop: 'additionAward'
    },
    {
      label: '之前金币',
      prop: 'beforeCoin'
    },
    {
      label: '金币变化量',
      prop: 'changeCoin'
    },
    {
      label: '之后金币',
      prop: 'afterCoin'
    },
    {
      label: '签到时间',
      prop: 'recordTime'
    }
  ]
}
