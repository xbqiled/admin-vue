export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menu: false,
  dic: [],
  column: [
	  {
      label: 'ID',
      prop: 'id',
      hide : true
    },
	  {
      label: '账号ID',
      search: true,
      prop: 'accountId'
    },
	  {
      label: '用户昵称',
      prop: 'nickName'
    },
	  {
      label: '上级账户ID',
      prop: 'promoterId'
    },
	  {
      label: '上级昵称',
      prop: 'promoterNickName'
    },
	  {
      label: '层级差',
      prop: 'level'
    },
	  {
      label: '返利值',
      prop: 'count'
    },
	  {
      label: '返利时间',
      prop: 'recordTime',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm:ss',
      valueFormat: 'timestamp',
    },
	  {
      label: '渠道编码',
      search: true,
      prop: 'channelNo'
    },
  ]
}
