export const DIC = {
  type: [{
    label: '人工加款',
    value: 1
  }, {
    label: '人工扣款',
    value: 2
  }, {
    label: '在线取款失败',
    value: 3
  }, {
    label: '在线取款',
    value: 4
  }, {
    label: '在线支付',
    value: 5
  }, {
    label: '快速入款',
    value: 6
  }, {
    label: '一般入款',
    value: 7
  }, {
    label: '反水加钱',
    value: 9
  }, {
    label: '反水回滚',
    value: 10
  }, {
    label: '代理返点加钱',
    value: 11
  }, {
    label: '代理返点回滚',
    value: 12
  }, {
    label: '多级代理返点加钱',
    value: 13
  }, {
    label: '多级代理返点回滚',
    value: 14
  }, {
    label: '代理占成加钱',
    value: 113
  }, {
    label: '三方额度转入系统额度',
    value: 15
  }, {
    label: '系统额度转入三方额度',
    value: 16
  }, {
    label: '站内额度转入',
    value: 87
  }, {
    label: '站内额度转出',
    value: 88
  }, {
    label: '存款取消',
    value: 55
  }, {
    label: '提款取消',
    value: 54
  }, {
    label: '晋级赠送',
    value: 78
  }, {
    label: '注册赠送',
    value: 79
  }, {
    label: '存款赠送',
    value: 80
  }, {
    label: '彩金扣除',
    value: 81
  }, {
    label: '体育投注',
    value: 201
  }, {
    label: '体育派奖',
    value: 202
  }, {
    label: '体育撤单',
    value: 203
  }, {
    label: '体育派奖回滚',
    value: 204
  }, {
    label: '彩票投注',
    value: 130
  }, {
    label: '彩票派奖',
    value: 131
  }, {
    label: '彩票撤单',
    value: 132
  }, {
    label: '彩票派奖回滚',
    value: 133
  }, {
    label: '参与彩票合买',
    value: 134
  }, {
    label: '彩票合买满员',
    value: 135
  }, {
    label: '彩票合买失效',
    value: 136
  }, {
    label: '彩票合买撤单',
    value: 137
  }, {
    label: '彩票合买派奖',
    value: 139
  }, {
    label: '彩票合买截止',
    value: 138
  }, {
    label: '六合彩投注',
    value: 140
  }, {
    label: '六合彩派奖',
    value: 141
  }, {
    label: '六合彩派奖回滚',
    value: 142
  }, {
    label: '六合彩撤单',
    value: 143
  }, {
    label: '活动中奖',
    value: 18
  }, {
    label: '现金兑换积分',
    value: 19
  }, {
    label: '积分兑换现金',
    value: 20
  }, {
    label: '三方转账失败',
    value: 17
  }, {
    label: '三方彩票增加',
    value: 1130
  }, {
    label: '三方彩票减少',
    value: 1131
  }, {
    label: '抢红包',
    value: 168
  }, {
    label: '发红包',
    value: 169
  }, {
    label: '红包金额返还',
    value: 170
  }, {
    label: '系统接口加款',
    value: 23
  }, {
    label: '余额宝盈利',
    value: 997
  }, {
    label: '余额宝转入',
    value: 998
  }, {
    label: '余额宝转出',
    value: 999
  }, {
    label: '首提',
    value: 996
  }, {
    label: '三方登录自动转入',
    value: 1000
  }, {
    label: '三方下线自动转出',
    value: 1001
  }]
}

export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menu: false,
  dic: [],
  column: [
    {
      label: '编号',
      prop: 'id',
      width: 120
    },
    {
      label: '用户昵称',
      prop: 'userName',
      width: 150
    },
    {
      label: '用户ID',
      prop: 'userId',
      width: 100
    },
    {
      label: '变动类型',
      prop: 'type',
      width: 180,
      dicData: DIC.type
    },
    {
      label: '变动前余额',
      prop: 'beforeMoney',
      width: 115,
      align: 'right'
    },
    {
      label: '变动金额',
      prop: 'money',
      width: 115,
      align: 'right'
    },
    {
      label: '变动后余额',
      prop: 'afterMoney',
      width: 115,
      align: 'right'
    },
    {
      label: '变动时间',
      prop: 'createDatetime',
      width: 155
    },
    {
      label: '订单号',
      prop: 'orderId',
      width: 120
    },
    {
      label: '备注',
      prop: 'remark',
      width: 300
    },
    {
      label: '渠道编码',
      prop: 'channelId'
    }
  ]
}
