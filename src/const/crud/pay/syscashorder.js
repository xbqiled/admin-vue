export const tableOption = {
  border: true,
  index: false,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menuWidth: 150,
  dic: [],
  column: [
	  {
      label: 'ID',
      prop: 'id',
      width: 100,
      align: 'center',
      hide:true,
      addVisdiplay: false
    },
	  {
      label: '订单号',
      search: true,
      prop: 'cashNo',
      width: 300
    },
	  {
      label: '用户ID',
      search: true,
      prop: 'userId',
      width: 150
    },
    {
      label: '申请人',
      width: 150,
      prop: 'createBy'
    },
	  {
      label: '提现金额',
      prop: 'cashFee',
      width: 150
    },
	  {
      label: '渠道编码',
      prop: 'channelId',
      width: 150
    },
	  {
      label: '状态',
      search: true,
      prop: 'status',
      width: 150,
      type: 'select',
      dicData:
        [{
          label: '提现申请',
          value: 0
        }, {
          label: '审批通过',
          value: 1
        }, {
          label: '审批失败',
          value: 2
        }]
    },
	  {
      label: '提现类型',
      prop: 'cashType',
      width: 150,
      type: 'select',
      dicData:
        [{
          label: '支付宝',
          value: 1
        }, {
          label: '银行卡',
          value: 2
        }, {
          label: '微信',
          value: 3
        }]
    },
	  {
      label: '用户账号',
      prop: 'account',
      width: 300
    },
	  {
      label: '姓名',
      prop: 'accountName',
      width: 150
    },
	  {
      label: '申请时间',
      width: 250,
      prop: 'createTime',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm:ss',
      valueFormat: 'datetime',
      editDisabled: true,
      addVisdiplay: false
    },
	  {
      label: '审批时间',
      width: 250,
      prop: 'approveTime',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm:ss',
      valueFormat: 'datetime',
      editDisabled: true,
      addVisdiplay: false
    },
	  {
      label: '审批人',
      width: 150,
      prop: 'approveBy'
    },
	  {
      label: '审批意见',
      prop: 'note',
      hide: true
    },
  ]
}
