export const tableOption = {
  border: true,
  index: false,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menuWidth: 150,
  dic: [],
  column: [
	  {
      label: 'ID',
      prop: 'id',
      width: 100,
      align: 'center',
      hide: true,
      addVisdiplay: false
    },
    {
      label: '订单号',
      search: true,
      prop: 'orderNo',
      width: 280,
    },
    {
      label: '用户ID',
      search: true,
      prop: 'userId',
      width: 100
    },
	  {
      label: '充值金额',
      prop: 'payFee',
      width: 100
    },
	  {
      label: '手续费',
      prop: 'poundage',
      width: 100
    },
	  {
      label: '渠道ID',
      prop: 'channelId',
      width: 150
    },
	  {
      label: '状态',
      prop: 'status',
      width: 150,
      search: true,
      type: 'select',
      dicData:
        [{
          label: '等待支付',
          value: 0
        }, {
          label: '支付成功',
          value: 1
        }, {
          label: '支付失败',
          value: 2
        }, {
          value: 3,
          label: '撤销支付'
        }, {
          value: 4,
          label: '扣款成功'
        }]
    },
    {
      label: '支付方式编码',
      prop: 'payId',
      width: 150,
      hide:true
    },
	  {
      label: '支付方式名称',
      prop: 'payName',
      width: 150
    },
	  {
      label: '姓名',
      prop: 'depositor',
      width: 150,
    },
	  {
      label: '用户账号',
      prop: 'account',
      width: 150,
    },
	  {
      label: '处理方式',
      prop: 'handlerType',
      type: 'select',
      width: 150,
      dicData:
        [{
          label: '手动处理',
          value: 0
        }, {
          label: '系统处理',
          value: 1
        }, {
          label: '手动上分',
          value: 2
        }, {
          value: 3,
          label: '手动撤销'
        }, {
          value: 4,
          label: '手动扣款'
        }, {
          value: 5,
          label: '手动赠送'
        }]
    },
	  {
      label: '创建时间',
      prop: 'createTime',
      type: 'datetime',
      width: 250,
      format: 'yyyy-MM-dd HH:mm:ss',
      valueFormat: 'datetime',
      editDisabled: true,
      addVisdiplay: false
    },
	  {
      label: '创建人',
      width: 150,
      prop: 'createBy'
    },
	  {
      label: '处理时间',
      prop: 'modifyTime',
      width: 250,
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm:ss',
      valueFormat: 'datetime',
      editDisabled: true,
      addVisdiplay: false
    },
	  {
      label: '处理人',
      width: 150,
      prop: 'modifyBy'
    },
	  {
      label: '备注',
      prop: 'note',
      hide:true
    },
  ]
}
