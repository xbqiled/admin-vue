export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
	  {
      label: '主键',
      prop: 'id'
    },
	  {
      label: '订单号',
      prop: 'cashOrder'
    },
	  {
      label: '渠道编码',
      prop: 'channelId'
    },
	  {
      label: '用户id',
      prop: 'userId'
    },
	  {
      label: '微信账号',
      prop: 'wechatAccount'
    },
	  {
      label: '状态，0:等待审核, 1,审核成功, 2,审核失败',
      prop: 'status'
    },
	  {
      label: '提现金额',
      prop: 'cashFee'
    },
	  {
      label: '创建人',
      prop: 'createBy'
    },
	  {
      label: '创建时间',
      prop: 'createTime'
    },
	  {
      label: '审批人',
      prop: 'modifyTime'
    },
	  {
      label: '如果系统处理，那么记录SYSTME, 手动处理记录编码',
      prop: 'modifyBy'
    },
	  {
      label: '备注',
      prop: 'note'
    },
  ]
}
