export default {
    option: [{
            label: '支付订单信息',
            prop: 'account',
            icon: 'el-icon-edit-outline',
            column: [
              {
                label: 'ID',
                prop: 'id',
                span:24,
                align: 'center'
              },
              {
                label: '订单号',
                span:24,
                prop: 'orderNo'
              },
              {
                label: '用户ID',
                prop: 'userId'
              },
              {
                label: '充值金额',
                prop: 'payFee'
              },
              {
                label: '手续费',
                prop: 'poundage'
              },
              {
                label: '渠道ID',
                prop: 'channelId'
              },
              {
                label: '状态',
                prop: 'status',
                type: 'select',
                dicData:
                  [{
                    label: '等待支付',
                    value: 0
                  }, {
                    label: '支付成功',
                    value: 1
                  }, {
                    label: '支付失败',
                    value: 2
                  }, {
                    value: 3,
                    label: '撤销支付'
                  }]
              },
              {
                label: '支付方式编码',
                prop: 'payId'
              },
              {
                label: '支付方式名称',
                prop: 'payName'
              },
              {
                label: '姓名',
                prop: 'depositor'
              },
              {
                label: '用户账号',
                prop: 'account'
              },
              {
                label: '处理方式',
                prop: 'handlerType',
                type: 'select',
                dicData:
                  [{
                    label: '手动处理',
                    value: 0
                  }, {
                    label: '系统处理',
                    value: 1
                  }, {
                    label: '手动上分',
                    value: 2
                  }]
              },
              {
                label: '创建时间',
                prop: 'createTime',
                type: 'datetime',
                format: 'yyyy-MM-dd HH:mm:ss',
                valueFormat: 'datetime',
                editDisabled: true,
                addVisdiplay: false
              },
              {
                label: '创建人',
                prop: 'createBy'
              },
              {
                label: '处理时间',
                prop: 'modifyTime',
                type: 'datetime',
                format: 'yyyy-MM-dd HH:mm:ss',
                valueFormat: 'datetime',
                editDisabled: true,
                addVisdiplay: false
              },
              {
                label: '处理人',
                prop: 'modifyBy'
              },
              {
                label: '备注',
                prop: 'note',
              },
            ]
        },
    ]
}
