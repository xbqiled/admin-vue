const DIC = {
  type: [{
    label: '在线支付',
    value: 0
  },
  {
    label: '银行打款',
    value: 1
  },
  {
    label: '支付宝打款',
    value: 2
  },
  {
    label: '微信打款',
    value: 3
  },
  {
    label: 'QQ打款',
    value: 4
  }
  ],
  pay_type: [{
      label: '微信公众账号',
      value: 0
    },
    {
      label: '微信扫码支付',
      value: 1
    },
    {
      label: '微信网页支付',
      value: 2
    },
    {
      label: '微信app支付',
      value: 3
    },
    {
      label: '微信转账支付',
      value: 4
    },
    {
      label: '支付宝扫码',
      value: 5
    },
    {
      label: '支付宝APP',
      value: 6
    },
    {
        label: '支付宝网页',
      value: 7
    },{
      label: '支付宝转账',
      value: 8
    },
    {
      label: 'QQ钱包Wap',
      value: 9
    },
    {
      label: 'QQ钱包手机扫码',
      value: 10
    },
    {
      label: 'QQ转账支付',
      value: 11
    },
    {
      label: '银联二维码',
      value: 12
    },
    {
      label: '网银B2B支付',
      value: 13
    },
    {
      label: '网银B2C支付',
      value: 14
    },
    {
      label: '无卡快捷支付',
      value: 15
    },
    {
      label: '银联H5快捷',
      value: 16
    },
    {
      label: '银行卡转账',
      value: 17
    },
    {
      label: '手动赠送',
      value: 18
    }
  ],
  state: [{
    label: '正常',
    value: 1
  },
    {
      label: '无效',
      value: 0
    }
  ],
  display_type:[{
      label: '全部终端',
      value: 0
    },
    {
      label: 'Android',
      value: 1
    },
    {
      label: 'Ios',
      value: 1
    },
    {
      label: '网页',
      value: 1
    }
  ],
  level:[{
    label: '无限制',
    value: "0"
  },
  {
      label: '一级',
      value: "1"
  },
    {
      label: '二级',
      value: "2"
    },
    {
      label: '三级',
      value: "3"
    },
    {
      label: '四级',
      value: "4"
    },
    {
      label: '五级',
      value: "5"
    },
    {
      label: '六级',
      value: "6"
    },
    {
      label: '七级',
      value: "7"
    },
    {
      label: '八级',
      value: "8"
    },
    {
      label: '九级',
      value: "9"
    },
    {
      label: '十级',
      value: "10"
    },
  ],
  isDef:[{
    label: '否',
    value: 0
  },
    {
      label: '是',
      value: 1
    }
  ],
}

export const tableOption = {
  border: true,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
	  {
      label: 'ID',
      prop: 'id',
      editDisabled: true,
      addVisdiplay: false
    },
    {
      label: '渠道ID',
      prop: 'channelId',
      search:true,
      width: 100,
      rules: [{
        required: true,
        message: '请输入站点信息',
        trigger: 'blur'
      }]
    },
    {
      label: '支付名称',
      prop: 'payName',
      search:true,
      width: 200,
      rules: [{
        required: true,
        message: '请输入支付名称',
        trigger: 'blur'
      }]
    },
    {
      label: '支付归类',
      prop: 'type',
      width: 180,
      type: 'select',
      dicData: DIC.type,
      rules: [{
        required: true,
        message: '支付类型不能为空',
        trigger: 'blur'
      }]
    },
    {
      label: '支付分类',
      prop: 'payType',
      type: 'select',
      width: 150,
      dicData: DIC.pay_type,
      rules: [{
        required: true,
        message: '请选择支付分类',
        trigger: 'blur'
      }],
    },
    {
      label: '通道类型',
      prop: 'platformType',
      width: 150,
      rules: [{
        required: true,
        message: '请输入通道类型',
        trigger: 'blur'
      }],
    },
	  {
      label: '商户号',
      prop: 'merchantCode',
      width: 150,
      rules: [{
        required: true,
        message: '请输入商户号',
        trigger: 'blur'
      }]
    },
	  {
      label: '商户秘钥',
      prop: 'merchantKey',
      width: 150,
      hide: true,
      rules: [{
        required: true,
        message: '请输入商户秘钥',
        trigger: 'blur'
      }]
    },
	  {
      label: '商户公钥',
      prop: 'publicKey',
      width: 250,
      hide: true,
      rules: [{
        required: true,
        message: '请输入商户公钥',
        trigger: 'blur'
      }]
    },
	  {
      label: '账号',
      prop: 'account',
      width: 150,
      rules: [{
        required: true,
        message: '请输入账号信息',
        trigger: 'blur'
      }]
    },
	  {
      label: '最小金额',
      prop: 'min',
      width: 100,
      type: 'number',
      rules: [{
        required: true,
        message: '请输入最小金额',
        trigger: 'blur'
      }]
    },
	  {
      label: '最大金额',
      prop: 'max',
      width: 100,
      type: 'number',
      rules: [{
        required: true,
        message: '请输入最大金额',
        trigger: 'blur'
      }]
    },
	  {
      label: '是否默认',
      prop: 'def',
      width: 100,
      type: 'select',
      dicData: DIC.isDef,
      rules: [{
        required: true,
        message: '请选择是否默认',
        trigger: 'blur'
      }]
    },
	  {
      label: '状态',
      prop: 'status',
      type: 'select',
      width: 100,
      dicData: DIC.state,
      rules: [{
        required: true,
        message: '请选择状态',
        trigger: 'blur'
      }]
    },
	  {
      label: '图标URL',
      prop: 'icon',
      width: 200,
      hide: true,
      editDisabled: true,
      addVisdiplay: false,
      rules: [{
        required: true,
        message: '请输入图标地址',
        trigger: 'blur'
      }]
    },
	  {
      label: '通道编码',
      prop: 'payPlatformId',
      width: 100,
      rules: [{
        required: true,
        message: '请输入通道编码',
        trigger: 'blur'
      }]
    },
	  {
      label: '支付网关',
      prop: 'payGetway',
      hide: true,
      width: 150,
      rules: [{
        required: true,
        message: '请输入支付网关',
        trigger: 'blur'
      }]
    },
    {
      label: 'AppId',
      prop: 'appKey',
      hide: true,
      rules: [{
        required: true,
        message: '请输入AppId信息',
        trigger: 'blur'
      }]
    },
	  {
      label: '序号',
      prop: 'sortNo',
      type: 'number',
      width: 100,
      rules: [{
        required: true,
        message: '请输入序号信息',
        trigger: 'blur'
      }]
    },
    {
      label: '返现倍率',
      prop: 'returnRate',
      type: 'number',
      width: 100,
      rules: [{
        required: true,
        message: '请输入返现倍率信息',
        trigger: 'blur'
      }]
    },
	  {
      label: '创建时间',
      prop: 'createTime',
      width: 150,
      align: 'center',
      type: 'date',
      format: 'yyyy-MM-dd HH:mm',
      valueFormat: 'yyyy-MM-dd hh:mm:ss',
      editDisabled: true,
      addVisdiplay: false
    },
    {
      label: '创建人',
      prop: 'createBy',
      width: 100,
      align: 'center',
      editDisabled: true,
      addVisdiplay: false
    },
    {
      label: '支付说明',
      prop: 'payDesc',
      hide: true,
      type: 'textarea',
      span: 24,
      maxRow: 4,
      minRow: 4
    },
    {
      label: '备注',
      prop: 'remark',
      hide: true,
      type: 'textarea',
      span: 24,
      maxRow: 4,
      minRow: 4
    }
  ]
}
