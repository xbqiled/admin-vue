const DIC = {
  pay_type: [{
    label: '银行支付',
    value: '0'
  },
  {
    label: '在线支付',
    value: '1'
  },
  {
    label: '快速支付',
    value: '1'
  }
  ],
  state:[{
    label: '正常',
    value: 1
  },
    {
      label: '无效',
      value: 0
    }
  ],
  isActive:[{
    label: '否',
    value: 0
  },
    {
      label: '是',
      value: 1
    }
  ],
}

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
    {
      label: 'ID',
      prop: 'id',
      width: 150,
      hide: true,
      editDisabled: true,
      addVisdiplay: false
    },
    {
      label: '组织名称',
      prop: 'company',
      width: 250,
      search:true,
      rules: [{
        required: true,
        message: '请输入组织名称',
        trigger: 'blur'
      }],
    },
    {
      label: '关键字',
      prop: 'platformKey',
      rules: [{
        required: true,
        message: '请输入关键字',
        trigger: 'blur'
      }],
    },
    {
      label: '处理方式',
      prop: 'handType',
      width: 150,
      type: 'select',
      dicData: [{
        label: '不处理',
        value: 0
      },{
        label: '返回PAYURL',
        value: 1
      }, {
        label: '页面提交',
        value: 2
      }, {
        label: '本地收银台',
        value: 3
      }, {
        label: '网页代码',
        value: 4
      }],
      rules: [{
        required: true,
        message: '请输入处理方式',
        trigger: 'blur'
      }],
    },
    {
      label: '请求方法',
      prop: 'method',
      width: 150,
      type: 'select',
      dicData: [{
        label: '不请求',
        value: 0
      }, {
        label: 'POST',
        value: 1
      }, {
        label: 'GET',
        value: 2
      }],
      rules: [{
        required: true,
        message: '请输入请求方法',
        trigger: 'blur'
      }],
    },
    {
      label: '状态',
      prop: 'status',
      width: 150,
      type: 'select',
      dicData: DIC.state,
      rules: [{
        required: true,
        message: '请选择状态',
        trigger: 'blur'
      }],
    },
    {
      label: '创建时间',
      prop: 'createTime',
      align: 'center',
      width: 180,
      type: 'date',
      format: 'yyyy-MM-dd HH:mm',
      valueFormat: 'yyyy-MM-dd hh:mm:ss',
      editDisabled: true,
      addVisdiplay: false
    },
    {
      label: '修改时间',
      prop: 'modifyTime',
      align: 'center',
      width: 180,
      type: 'date',
      format: 'yyyy-MM-dd HH:mm',
      valueFormat: 'yyyy-MM-dd hh:mm:ss',
      editDisabled: true,
      addVisdiplay: false
    },
    {
      label: '创建用户',
      prop: 'createBy',
      editDisabled: true,
      addVisdiplay: false
    },
    {
      label: '修改用户',
      prop: 'modifyBy',
      editDisabled: true,
      addVisdiplay: false
    },
    {
      label: '序号',
      prop: 'sortNo',
      type: 'number',
      rules: [{
        required: true,
        message: '请输入公司名称',
        trigger: 'blur'
      }],
    },
    {
      label: '说明',
      prop: 'note',
      type: 'textarea',
      span: 24,
      maxRow: 4,
      minRow: 4,
    },
  ]
}
