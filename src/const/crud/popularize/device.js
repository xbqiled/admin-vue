export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
	  {
      label: '主键',
      prop: 'id',
      hide: true,
      editDisabled: true,
      addVisdiplay: false
    },
	  {
      label: '渠道ID',
      search:true,
      prop: 'channelId',
      rules: [{
        required: true,
        message: '渠道编码不能为空',
        trigger: 'blur'
      }]
    },
	  {
      label: 'IP地址',
      search:true,
      prop: 'ip',
      rules: [{
        required: true,
        message: 'IP地址不能为空',
        trigger: 'blur'
      }]
    },
	  {
      label: '推广人ID',
      prop: 'promoterId',
      search:true,
      rules: [{
        required: true,
        message: '推广人编码不能为空',
        trigger: 'blur'
      }]
    },
	  {
      label: '参数信息',
      prop: 'paraInfo',
      rules: [{
        required: true,
        message: '参数信息不能为空',
        trigger: 'blur'
      }]
    },
    {
      label: '系统信息',
      prop: 'os',
      search:true,
      rules: [{
        required: true,
        message: '系统信息不能为空',
        trigger: 'blur'
      }]
    },
	  {
      label: '比较次数',
      prop: 'compareCount',
      rules: [{
        required: true,
        message: '比较次数不能为空',
        trigger: 'blur'
      }]
    },
	  {
      label: '设备信息',
      prop: 'deviceInfo',
      hide: true,
      addVisdiplay: false,
      rules: [{
        required: true,
        message: '设备信息不能为空',
        trigger: 'blur'
      }]
    },
	  {
      label: '状态',
      prop: 'status',
      type: 'select',
      dicData: [{
        label: '拉黑',
        value: 0
      }, {
        label: '正常',
        value: 1
      }],
      rules: [{
        required: true,
        message: '状态信息不能为空',
        trigger: 'blur'
      }]
    },
	  {
      label: '下载时间',
      prop: 'createTime',
      editDisabled: true,
      addVisdiplay: false
    },
  ]
}
