export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menuWidth: 150,
  dic: [],
  column: [
	  {
      label: '平台',
      prop: 'name'
    },
	  {
      label: '开始时间',
      prop: 'start'
    },
    {
      label: '结束时间',
      prop: 'finish'
    },
    {
      label: '状态',
      prop: 'status',
      type: 'select',
      dicData:
        [ {
          label: '维护',
          value: 1
        }, {
          label: '正常',
          value: 2
        }]
    }
  ]
}
