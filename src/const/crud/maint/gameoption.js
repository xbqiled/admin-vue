export default {
  option: [{
    label: '游戏房间信息',
    prop: 'gameoption',
    icon: 'el-icon-edit-outline',
    column: [
      {
        label: '游戏名称',
        prop: 'gamekindname',
        span: 24,
        width: 150
      },
      {
        label: '开始时间',
        prop: 'start',
        span: 12,
        width: 250,
        type: 'datetime'
      },
      {
        label: '结束时间',
        prop: 'finish',
        span: 12,
        width: 250,
        type: 'datetime'
      },
      {
        label: '玩家数量',
        width: 150,
        prop: 'num'
      },
      {
        label: '房间编码',
        prop: 'roomId',
        width: 150
      },
      {
        label: '状态',
        search: true,
        prop: 'status',
        width: 150,
        type: 'select',
        dicData:
          [ {
            label: '维护',
            value: 1
          }, {
            label: '正常',
            value: 2
          }]
      },
      {
        label: '房间名称',
        prop: 'phonegamename',
        span: 24,
        width: 200
      }
    ]
  }
  ]
}
