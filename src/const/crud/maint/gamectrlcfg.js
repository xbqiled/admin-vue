export const tableOption = {
  border: true,
  index: false,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
	  {
      label: '游戏id',
      prop: 'gameAtomTypeId'
    },
    {
      label: '游戏名称',
      prop: 'gameKindName'
    },
	  {
      label: '配置时间',
      prop: 'cfgTime'
    }
  ]
}
