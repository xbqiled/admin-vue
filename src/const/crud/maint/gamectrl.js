export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menu: false,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
    {
      label: '房间编码',
      prop: 'gameAtomTypeId'
    },
	  {
      label: '房间名称',
      prop: 'phoneGameName'
    },
    {
      label: '游戏编码',
      prop: 'gameKindType'
    },
    {
      label: '游戏名称',
      prop: 'gameKindName'
    },
    {
      label: '安全下限值',
      prop: 'safeDown'
    },
    {
      label: '安全上限值',
      prop: 'safeUp'
    },
    {
      label: '危险下限值',
      prop: 'dangerDown'
    },
    {
      label: '危险上限值',
      prop: 'dangerUp'
    }
  ]
}
