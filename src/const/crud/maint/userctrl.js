export const tableOption = {
  border: true,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menuWidth: 100,
  dic: [],
  column: [
	  {
      label: '用户ID',
      prop: 'accountId',
      width: 120,
      search: true,
    },
    {
      label: '游戏名称',
      width: 120,
      prop: 'gameKindName'
    },
	  {
      label: '房间ID',
      prop: 'gameId',
      hide: true,
      search: true,
    },
    {
      label: '房间名称',
      width: 240,
      prop: 'phoneGameName'
    },
    {
      label: '渠道ID',
      prop: 'channelId',
      width: 120,
      search: true,
      hide: true
    },
    {
      label: '渠道名称',
      width: 120,
      prop: 'channelName'
    },
    {
      label: '类型',
      prop: 'type',
      width: 100,
      type: 'select',
      dicData:
        [{
          label: '不控制',
          value: 0
        }, {
          label: '放水',
          value: 1
        }, {
          label: '缩水',
          value: 2
        }]
    },
    {
      label:'图标',
      prop:'iconUrl',
      type:'upload',
      imgWidth: 50,
      imgHeight: 50,
      hide: true,
      listType:'picture-img'
    },
    {
      label: '下注限额',
      width: 120,
      prop: 'betLimit'
    },
    {
      label: '权重/概率',
      width: 120,
      prop: 'rate'
    },
    {
      label: '控制局数',
      width: 100,
      prop: 'round'
    },
    {
      label: '控制总金额',
      width: 100,
      prop: 'tCtltotalscore'
    },
    {
      label: '历史总局数',
      width: 100,
      prop: 'totalRound'
    },
    {
      label: '历史赢取',
      width: 100,
      prop: 'histWin'
    },
    {
      label: '今日赢取',
      width: 100,
      prop: 'todayWin'
    },
    {
      label: '今日玩次数',
      width: 100,
      prop: 'totalPlayCount'
    },
    {
      label: '数据最后更新',
      width: 180,
      prop: 'todayRsfTime',
      formatter: (row) => {
        var value = row['todayRsfTime']
        if (value == null || value == undefined || value == '' || value == null || value == '1970-01-01 08:00:00') {
          return ''
        }
        return value
      }
    }
  ]
}
