export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
	  {
      label: '唯一key',
      prop: 'keyId'
    },
    {
      label: '控制类型',
      prop: 'cfgType'
    },
	  {
      label: '配置时间',
      prop: 'cfgTime'
    }
  ]
}
