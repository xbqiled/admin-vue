export default {
  option: [{
    label: '控制配置信息',
    prop: 'gameoption',
    icon: 'el-icon-edit-outline',
    column: [
      {
        label: '6倍间隔',
        prop: 'sixInterval',
        span: 24,
        width: 300
      },
      {
        label: '8倍间隔',
        prop: 'eightInterval',
        width: 300,
        span: 24
      },
      {
        label: '12倍间隔',
        prop: 'twelveInterval',
        span: 24,
        width: 300
      },
      {
        label: '全局间隔',
        prop: 'globalInterval',
        span: 24,
        width: 300
      },
      {
        label: '通吃间隔',
        prop: 'killAllInterval',
        span: 24,
        width: 300
      }
    ]
  }
  ]
}
