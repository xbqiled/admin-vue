export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menuWidth: 150,
  dic: [],
  column: [
    {
      label: '游戏名称',
      prop: 'gamekindname'
    },
	  {
      label: '开始时间',
      prop: 'start',
      type: 'datetime'
    },
	  {
      label: '结束时间',
      prop: 'finish',
      type: 'datetime'
    },
    {
      label: '玩家数量',
      prop: 'num'
    },
	  {
      label: '房间编码',
      prop: 'roomId'
    },
    {
      label: '房间名称',
      prop: 'phonegamename',
      width: 300
    },
	  {
      label: '状态',
      search: true,
      prop: 'status',
      type: 'select',
      dicData:
        [ {
          label: '维护',
          value: 1
        }, {
          label: '正常',
          value: 2
        }]
    }
  ]
}
