export default {
  option: [{
    label: '平台信息',
    prop: 'gameoption',
    icon: 'el-icon-edit-outline',
    column: [
      {
        label: '平台',
        prop: 'name',
        span: 24,
        width: 300
      },
      {
        label: '开始时间',
        prop: 'start',
        width: 280,
        span: 12,
        type: 'datetime'
      },
      {
        label: '结束时间',
        prop: 'finish',
        span: 12,
        width: 280,
        type: 'datetime'
      },
      {
        label: '状态',
        prop: 'status',
        width: 300,
        type: 'select',
        dicData:
          [ {
            label: '维护',
            value: 1
          }, {
            label: '正常',
            value: 2
          }]
      }
    ]
  }
  ]
}
