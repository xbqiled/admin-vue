export const tableOption = {
  border: true,
  index: false,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menuWidth: 100,
  dic: [],
  column: [
	  {
      label: 'ID',
      prop: 'id',
      hide: true
    },
	  {
      label: '渠道编码',
      prop: 'channelId'
    },
	  {
      label: '头部URL',
      prop: 'headUrl'
    },
	  {
      label: '中部URL',
      prop: 'bodyUrl'
    },
	  {
      label: '底部URL',
      prop: 'footUrl'
    },
    {
      label: '背景URL',
      prop: 'backgroundUrl'
    },
	  {
      label: '创建时间',
      prop: 'createTime'
    },
	  {
      label: '创建人',
      prop: 'createBy'
    },
    {
      label: '修改时间',
      prop: 'modifyTime'
    }
  ]
}
