export default {
  option: [{
    label: 'ToKen信息',
    prop: 'mail',
    icon: 'el-icon-edit-outline',
    column: [{
      label: '渠道编码',
      span: 24,
      prop: 'channelId',
    }, {
      label: 'accessToken',
      span: 24,
      prop: 'accessToken',
    }, {
      label: 'sessionKey',
      span: 24,
      prop: 'sessionKey',
    }, {
      label: 'scope',
      span: 24,
      prop: 'scope',
    }, {
      label: 'refreshToken',
      span: 24,
      prop: 'refreshToken',
    }, {
      label: 'sessionSecret',
      span: 24,
      prop: 'sessionSecret',
    },{
      label: 'expiresTime',
      span: 24,
      prop: 'expiresTime',
    }, {
      label: 'createTime',
      span: 24,
      prop: 'createTime',
    }
    ]
  }
  ]
}
