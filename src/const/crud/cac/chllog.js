export const tableOption = {
  border: true,
  index: false,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menu: false,
  dic: [],
  column: [
	  {
      label: 'ID',
      prop: 'id',
      hide: true
    },
	  {
      label: '账号id',
      prop: 'userId'
    },
	  {
      label: '账号名称',
      prop: 'username'
    },
	  {
      label: '用户操作',
      prop: 'operation'
    },
	  {
      label: '请求方法',
      prop: 'method'
    },
	  {
      label: '请求参数',
      prop: 'params'
    },
	  {
      label: '渠道编码',
      prop: 'channelId'
    },
	  {
      label: '执行时长(毫秒)',
      prop: 'time'
    },
	  {
      label: 'IP地址',
      prop: 'ip'
    },
	  {
      label: '创建时间',
      prop: 'createDate'
    },
  ]
}
