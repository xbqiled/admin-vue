export const tableOption = {
  border: true,
  index: false,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  menuWidth: 100,
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
	  {
      label: '邮件ID',
      prop: 'id',
      align: 'center',
      width: 100
    },
	  {
      label: '收件人ID',
      prop: 'accountId',
      search:true
    },
    {
      label: '收件人名称',
      prop: 'accountname'
    },
    {
      label: '收件人昵称',
      prop: 'nickname'
    },
	  {
      label: '邮件标题',
      prop: 'title',
      search:true
    },
	  {
      label: '发送人',
      prop: 'sender',
      align: 'center',
    },
	  {
      label: '状态',
      prop: 'state',
      align: 'center',
      type: 'select',
      dicData:
        [{
          label: '未读',
          value: 1
        }, {
          label: '已读',
          value: 2
        }, {
          label: '领取',
          value: 3
        }]
    },
	  {
      label: '发送日期',
      prop: 'senddate',
      align: 'center',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm:ss',
      valueFormat: 'timestamp',
      width: 250
    },
	  {
      label: '接收日期',
      prop: 'recvdate',
      align: 'center',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm:ss',
      valueFormat: 'timestamp',
      width: 250
    }
  ]
}
