export default {
  option: [{
    label: '绑定提现信息',
    prop: 'account',
    icon: 'el-icon-edit-outline',
    column: [{
      label: '支付宝账号',
      span: 12,
      prop: 'aliPayId'
    }, {
      label: '支付宝名称',
      span: 12,
      prop: 'aliRealName',
    }, {
      label: '银行账号',
      span: 12,
      prop: 'bankPayId'
    }, {
      label: '银行账户名称',
      span: 12,
      prop: 'bankRealName'
    }, {
      label: '银行名称',
      span: 12,
      prop: 'bank'
    }, {
      label: '分行名称',
      span: 12,
      prop: 'subBank'
    }]
  }]
}
