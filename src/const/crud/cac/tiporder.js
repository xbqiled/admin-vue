export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
	  {
      label: '主键编码',
      prop: 'id'
    },
	  {
      label: '订单编号',
      prop: 'orderNo'
    },
	  {
      label: '类型: 1, 支付订单 2,提现订单',
      prop: 'type'
    },
	  {
      label: '状态: 1, 已提醒',
      prop: 'status'
    },
	  {
      label: '已提醒次数',
      prop: 'tipCount'
    },
	  {
      label: '创建时间',
      prop: 'createTime'
    },
  ]
}
