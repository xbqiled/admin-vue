export const tableOption = {
  border: true,
  index: false,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
	  {
      label: '名称',
      prop: 'name',
      width: 180
    },
	  {
      label: '通道编码',
      prop: 'platformId',
      width: 120
    },
    {
      label: '通道名称',
      prop: 'platformName',
      width: 150
    },
	  {
      label: '是否激活',
      prop: 'isActivate',
      type: 'select',
      width: 100,
      dicData:
        [{
          label: '激活',
          value: 1
        }, {
          label: '失效',
          value: 0
        }]
    },
	  {
      label: '描述',
      width: 180,
      prop: 'description'
    },
	  {
      label: '渠道编码',
      width: 180,
      prop: 'channelId'
    },
	  {
      label: '当日已推送次数',
      width: 150,
      prop: 'dayCount'
    },
	  {
      label: '每日可推送次数',
      width: 150,
      prop: 'dayMaxCount'
    },
	  {
      label: '总计推送次数',
      width: 150,
      prop: 'totalCount'
    },
	  {
      label: '创建时间',
      prop: 'createTimeStr',
      width: 180,
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm:ss',
      valueFormat: 'datetime'
    },
	  {
      label: '创建人',
      prop: 'createBy',
      width: 180
    },
	  {
      label: '最后一次更新时间',
      prop: 'lastUpdateTimeStr',
      type: 'datetime',
      width: 180,
      format: 'yyyy-MM-dd HH:mm:ss',
      valueFormat: 'datetime'
    },
  ]
}
