export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
	  {
      label: 'ID',
      prop: 'id',
      addVisdiplay: false
    },
	  {
      label: '渠道ID',
      prop: 'channelId',
      search: true,
      rules: [{
        required: true,
        message: '渠道ID不能为空',
        trigger: 'blur'
      }],
    },
	  {
      label: '更新地址',
      prop: 'updateUrl',
      rules: [{
        required: true,
        message: '更新地址不能为空',
        trigger: 'blur'
      }],
    },
	  {
      label: '是否删除',
      prop: 'isClear',
      type: 'select',
      rules: [{
        required: true,
        message: '是否不能为空',
        trigger: 'blur'
      }],
      dicData:
        [{
          label: '不删除',
          value: '0'
        }, {
          label: '删除',
          value: '1'
        }]
    },
	  {
      label: '版本',
      prop: 'version',
      rules: [{
        required: true,
        message: '更版本不能为空',
        trigger: 'blur'
      }],
    },
	  {
      label: '创建时间',
      prop: 'createTime',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm:ss',
      valueFormat: 'datetime',
      editDisabled: true,
      addVisdiplay: false
    },
	  {
      label: '创建人',
      prop: 'createBy',
      editDisabled: true,
      addVisdiplay: false
    },
	  {
      label: '修改时间',
      prop: 'modifyTime',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm:ss',
      valueFormat: 'datetime',
      editDisabled: true,
      addVisdiplay: false
    },
	  {
      label: '修改人',
      prop: 'modifyBy',
      editDisabled: true,
      addVisdiplay: false
    },
  ]
}
