export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
	  {
      label: '主键',
      prop: 'id'
    },
	  {
      label: '渠道编码',
      prop: 'channelId'
    },
	  {
      label: '',
      prop: 'appId'
    },
	  {
      label: '创建人',
      prop: 'createBy'
    },
	  {
      label: '创建时间',
      prop: 'createTime'
    },
  ]
}
