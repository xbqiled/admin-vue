export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
	  {
      label: '公告ID',
      prop: 'tid',
      search: true,
      rules: [{
        required: true,
        message: '请输入公告信息',
        trigger: 'blur'
      }]
    },
    {
      label: '公告类型',
      prop: 'ttype',
      type: 'select',
      dicData:
        [{
          label: '公告栏',
          value: 1
        }, {
          label: '跑马灯',
          value: 2
        }]
    },
	  {
      label: '公告标题',
      search: true,
      prop: 'ttitle',
    },
    {
      label: '创建时间',
      prop: 'createtime',
      align: 'center',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm',
      valueFormat: 'timestamp'
    }
  ]
}
