export const tableOption = {
  border: true,
  index: false,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menuWidth: 150,
  dic: [],
  column: [
	  {
      label: '主键Id',
      prop: 'id',
      hide: true,
      labelWidth: 150,
      addVisdiplay: false,
      editDisabled: true,
    },
	  {
      label: '类型',
      type: 'select',
      prop: 'type',
      labelWidth: 150,
      rules: [{
        required: true,
        message: '请选择配置类型',
        trigger: 'blur'
      }],
      dicData:
        [ {
          label: '百度',
          value: 1
        }]
    },
	  {
      label: 'client_id',
      prop: 'clientId',
      labelWidth: 150,
      rules: [{
        required: true,
        message: '请输入clientId信息',
        trigger: 'blur'
      }]
    },
	  {
      label: 'client_secret',
      prop: 'clientSecret',
      labelWidth: 150,
      rules: [{
        required: true,
        message: '请输入clientSecret信息',
        trigger: 'blur'
      }]
    },
	  {
      label: 'grant_type',
      prop: 'grantType',
      labelWidth: 150,
      rules: [{
        required: true,
        message: '请输入grantType信息',
        trigger: 'blur'
      }]
    },
	  {
      label: '渠道编码',
      prop: 'channelId',
      labelWidth: 150,
      editDisabled: true,
      search: true,
      rules: [{
        required: true,
        message: '请输入渠道编码信息',
        trigger: 'blur'
      }]
    },
	  {
      label: '创建时间',
      prop: 'createTime',
      labelWidth: 150,
      addVisdiplay: false,
      editDisabled: true
    },
	  {
      label: '创建人',
      prop: 'createBy',
      labelWidth: 150,
      addVisdiplay: false,
      editDisabled: true
    },
  ]
}
