export const tableOption = {
  border: true,
  index: false,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menu:true,
  menuWidth: 100,
  dic: [],
  column: [
	  {
      label: '主键',
      prop: 'id',
      hide: true
    },
	  {
      label: '渠道编码',
      width: 100,
      search: true,
      prop: 'channelId'
    },
	  {
      label: 'accessToken',
      prop: 'accessToken'
    },
	  {
      label: 'sessionKey',
      prop: 'sessionKey'
    },
	  {
      label: 'scope',
      prop: 'scope',
      hide: true
    },
	  {
      label: 'refreshToken',
      prop: 'refreshToken'
    },
	  {
      label: 'sessionSecret',
      prop: 'sessionSecret',
      hide: true
    },
	  {
      label: '失效时间',
      width: 180,
      prop: 'expiresTime'
    },
	  {
      label: '创建时间',
      width: 180,
      prop: 'createTime'
    },
  ]
}
