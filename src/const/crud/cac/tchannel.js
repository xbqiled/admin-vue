export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
	  {
      label: 'ID',
      prop: 'id',
      hide: true,
      addVisdiplay: false,
      editDisabled: true,
    },
	  {
      label: '渠道标识',
      prop: 'channelId',
      search:true,
      editDisabled: true,
      rules: [{
        required: true,
        message: '请输入渠道标识',
        trigger: 'blur'
      }]
    },
	  {
      label: '渠道名称',
      prop: 'channelName',
      search:true,
      rules: [{
        required: true,
        message: '请输入渠道名称',
        trigger: 'blur'
      }]
    },
	  {
      label: '渠道类型',
      prop: 'type',
      type: 'select',
      dicData: [{
        label: '自有渠道',
        value: 0
      }, {
        label: '社会渠道',
        value: 1
      }],
      rules: [{
        required: true,
        message: '请选择渠道类型',
        trigger: 'blur'
      }]
    },
	  {
      label: '状态',
      prop: 'state',
      type: 'select',
      dicData: [{
        label: '正常',
        value: 0
      }, {
        label: '关闭',
        value: 1
      }],
      rules: [{
        required: true,
        message: '请选择状态',
        trigger: 'blur'
      }]
    },
    {
      label: '渠道图标',
      prop: 'iconUrl',
      hide: true,
      rules: [{
        required: true,
        message: '图标URL信息不能为空',
        trigger: 'blur'
      }]
    },
	  {
      label: '创建时间',
      width: 250,
      prop: 'createTime',
      type: 'datetime',
      editDisabled: true,
      format: 'yyyy-MM-dd HH:mm',
      valueFormat: 'yyyy-MM-dd hh:mm:ss',
      addVisdiplay: false
    },
	  {
      label: '创建人',
      prop: 'createBy',
      align: 'center',
      editDisabled: true,
      addVisdiplay: false
    },
	  {
      label: '描述',
      prop: 'note',
      type: 'textarea',
      hide: true,
      minRows: 6,
      span: 24
    },
  ]
}
