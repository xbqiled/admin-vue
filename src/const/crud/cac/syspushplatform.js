export const tableOption = {
  border: true,
  index: false,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
	  {
      label: '名称',
      prop: 'name',
      width: 150
    },
	  {
      label: '类型',
      prop: 'type',
      type: 'select',
      width: 150,
      dicData:
        [{
          label: '第三方',
          value: 1
        }, {
          label: '集成',
          value: 0
        }]
    },
	  {
      label: '类名',
      prop: 'className',
      width: 150
    },
	  {
      label: '状态',
      prop: 'status',
      type: 'select',
      width: 150,
      dicData:
        [{
          label: '正常',
          value: 1
        }, {
          label: '失效',
          value: 0
        }]
    },
	  {
      label: '描述',
      prop: 'description',
      width: 250,
    },
	  {
      label: '创建时间',
      prop: 'createTimeStr',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm:ss',
      valueFormat: 'datetime'
    },
	  {
      label: '创建人',
      prop: 'createBy'
    },
  ]
}
