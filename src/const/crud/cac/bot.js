export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  column: [
	  {
      label: '账号ID',
      prop: 'tid',
      search:true,
      editDisabled: true,
      addVisdiplay: false,
      rules: [{
        required: true,
        message: '账号ID不能为空',
        trigger: 'blur'
      }]
    },
	  {
      label: 'BOT昵称',
      prop: 'tnick',
      search:true,
      rules: [{
        required: true,
        message: 'BOT昵称不能为空',
        trigger: 'blur'
      }]
    },
	  {
      label: '头像ID',
      prop: 'tavatarid',
      editDisabled: true,
      addVisdiplay: false,
      rules: [{
        required: true,
        message: 'BOT头像不能为空',
        trigger: 'blur'
      }]
    },
  ]
}
