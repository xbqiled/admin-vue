export const configTableOption = {
  border: true,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
    {
      label: '游戏编码',
      prop: 'gameKindType'
    },
    {
      label: '房间编码',
      prop: 'gameAtomTypeId'
    },
	  {
      label: '游戏名称',
      prop: 'gameKindName',
    },
	  {
      label: '房间名称',
      prop: 'phoneGameName',
    },
	  {
      label: '渠道名称',
      prop: 'channelName'
    },
	  {
      label: '状态',
      prop: 'state'
    }
  ]
}
