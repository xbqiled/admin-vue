export const tableOption = {
  border: true,
  index: false,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
	  {
      label: 'ID',
      prop: 'id',
      labelWidth: 150,
      hide: true,
      addVisdiplay: false,
      editDisabled: true,
    },
    {
      label: '配置ID',
      prop: 'configId',
      labelWidth: 150,
      type: 'select',
      dicData: [{
        label: '阿里大鱼',
        value: 1
      }, {
        label: '云信互联',
        value: 2
      }, {
        label: '容联云',
        value: 3
      }],
      rules: [
        {
          required: true,
          message: '请输入归属配置ID',
          trigger: 'blur'
        }
      ]
    },
	  {
      label: '渠道编码',
      search:true,
      prop: 'channelId',
      labelWidth: 150,
      rules: [
        {
          required: true,
          message: '请输入归属渠道编码',
          trigger: 'blur'
        }
      ]
    },
	  {
      label: '模板编码',
      prop: 'templateCode',
      search:true,
      labelWidth: 150,
      rules: [{
        required: true,
        message: '请选择模板类型',
        trigger: 'blur'
      }]
    },
    {
      label: '服务端编码',
      prop: 'serverCode',
      labelWidth: 150,
      rules: [{
        required: true,
        message: '服务端编码',
        trigger: 'blur'
      }]
    },
	  {
      label: '模板类型',
      prop: 'templateType',
      labelWidth: 150,
      type: 'select',
      dicData: [{
        label: '验证码',
        value: 0
      }, {
        label: '通知',
        value: 1
      }, {
        label: '推广',
        value: 2
      }],
      rules: [{
        required: true,
        message: '请选择模板类型',
        trigger: 'blur'
      }]
    },
	  {
      label: '状态',
      prop: 'state',
      type: 'select',
      labelWidth: 150,
      dicData: [{
        label: '正常',
        value: 0
      }, {
        label: '失效',
        value: 1
      }],
      rules: [{
        required: true,
        message: '请选择状态',
        trigger: 'blur'
      }]
    },
	  {
      label: '创建时间',
      prop: 'createTime',
      type: 'datetime',
      labelWidth: 150,
      editDisabled: true,
      format: 'yyyy-MM-dd HH:mm',
      valueFormat: 'yyyy-MM-dd hh:mm:ss',
      addVisdiplay: false
    },
	  {
      label: '创建人',
      prop: 'createBy',
      labelWidth: 150,
      editDisabled: true,
      addVisdiplay: false
    },
    {
      label: '模板内容',
      prop: 'templateName',
      type: 'textarea',
      labelWidth: 150,
      span: 24,
      hide: true,
      rules: [{
        required: true,
        message: '请选择模板内容',
        trigger: 'blur'
      }]
    },
  ]
}
