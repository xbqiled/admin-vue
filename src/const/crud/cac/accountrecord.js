export const tableOption = {
  border: true,
  index: false,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menuWidth: 1,
  dic: [],
  column: [
	  {
      label: '主键',
      prop: 'id',
      hide: false
    },
	  {
      label: '业务标识',
      prop: 'bizId'
    },
	  {
      label: '渠道编码',
      prop: 'channelId'
    },
	  {
      label: '会计项目',
      prop: 'accountItem',
      type:'select',
      dicData:
        [{
          label: '加款',
          value: 1
        },
        {
          label: '扣款',
          value: 2
        }]
    },
	  {
      label: '状态',
      prop: 'status',
      type:'select',
      dicData:
        [{
          label: '成功',
          value: 1
        }]
    },
	  {
      label: '类型编码',
      prop: 'gameType',
      type:'select',
      dicData:
        [{
          label: '真人',
          value: 1
        }]
    },
    {
      label: '变化前金额',
      prop: 'beforeAmount'
    },
	  {
      label: '变化金额',
      prop: 'changeAmount'
    },
	  {
      label: '变化后金额',
      prop: 'afterAmount'
    },
	  {
      label: '操作时间',
      prop: 'createTime'
    },
	  {
      label: '操作人',
      prop: 'createBy'
    }
  ]
}
