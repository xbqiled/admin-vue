export default {
    option: [{
            label: '推送信息',
            prop: 'account',
            icon: 'el-icon-edit-outline',
            column: [{
                label: '消息编码',
                prop: 'msgId'
            }, {
                label: '渠道编码',
                prop: 'channelId',
            },{
                label: '平台',
                type:'select',
                prop: 'platform',
                dicData:
                  [{
                    value: 'all',
                    label: '全部平台'
                  }, {
                    value: 'ios',
                    label: 'ios'
                  }, {
                    value: 'android',
                    label: 'android'
                  }, {
                    value: 'winphone',
                    label: 'winphone'
                  }]
            },
            {
              label: '发送时间',
              prop: 'timeType',
              type:'select',
              dicData: [ {
                value:  0,
                label: '立即'
              }, {
                value: 1,
                label: '定时'
              }]
            }, {
                label: '定时时间',
                prop: 'retainTime',
            }, {
                label: '发送类型',
                prop: 'sendType',
                type: 'select',
                dicData:
                  [{
                    value:  0,
                    label: '全体'
                  }, {
                    value:  1,
                    label: '别名'
                  }]
            }, {
                label: '指定设备',
                prop: 'audience'
            }, {
                label: '保留时长',
                prop: 'timeLong'
            }, {
              label: '发送系统',
              prop: 'sendSys',
              type: 'select',
              dicData:
                [{
                  value: 0,
                  label: '总控'
                }, {
                  value: 1,
                  label: '渠道'
                }]
            }, {
              label: '通道名称',
              prop: 'platformName'
            }, {
              label: '配置名称',
              prop: 'configName'
            }, {
              label: '推送时间',
              prop: 'pushTime',
              type: 'datetime',
              format: 'yyyy-MM-dd HH:mm:ss',
              valueFormat: 'datetime'
            }, {
              label: '推送操作人',
              prop: 'pushBy'
            }, {
              label: '标题',
              span: 24,
              prop: 'title'
            }, {
              label: '内容体',
              span: 24,
              prop: 'content'
            }]
        }
    ]
}
