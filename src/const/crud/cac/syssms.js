export const tableOption = {
  border: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  index: false,
  menuWidth: 150,
  dic: [],
  column: [
	  {
      label: 'ID',
      prop: 'id',
      align: 'center',
      addVisdiplay: false
    },
    {
      label: '渠道编码',
      search:true,
      prop: 'channelId',
      rules: [
        {
          required: true,
          message: '请输入归属渠道编码',
          trigger: 'blur'
        }
      ]
    },
	  {
      label: '号码',
      search:true,
      prop: 'phoneNumber',
      rules: [{
        required: true,
        message: '请输入短信接收号码',
        trigger: 'blur'
      }]
    },
    {
      label: '模板编码',
      prop: 'templateCode'
    },
	  {
      label: '创建人',
      prop: 'createUser',
      align: 'center',
      addVisdiplay: false
    },
	  {
      label: '创建时间',
      prop: 'createTime',
      align: 'center',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm',
      valueFormat: 'timestamp',
      editDisabled: true,
      addVisdiplay: false
    },
	  {
      label: '状态',
      prop: 'state',
      type: 'select',
      addVisdiplay: false,
      dicData: [{
        label: '发送中',
        value: '0'
      }, {
        label: '发送成功',
        value: '1'
      }, {
        label: '发送失败',
        value: '2'
      }]
    },
    {
      label: '内容',
      prop: 'content',
      type: 'textarea',
      addVisdiplay: false,
      minRows: 3,
      span: 24
    },{
      label: '参数',
      prop: 'parameter',
      type: 'textarea',
      minRows: 3,
      span: 24
    }
  ]
}
