export default {
    option: [{
            label: '用户信息',
            prop: 'account',
            icon: 'el-icon-edit-outline',
            column: [{
                label: '用户ID',
                prop: 'tAccountid'
            }, {
                label: '用戶名',
                prop: 'tAccountname',
            }, {
                label: '昵称',
                prop: 'tNickname'
            }, {
                label: '正式账号',
                type:'select',
                prop: 'tBisregularaccount',
                dicData:
                  [{
                    label: '非正式',
                    value: 0
                  }, {
                    label: '正式账号',
                    value: 1
                  }]
            },
            {
              label: '用戶状态',
              prop: 'tAccountstate',
              type:'select',
              dicData: [{
                label: '正常',
                value: 0
              }, {
                label: '临时冻结',
                value: 1
              }, {
                label: '永久冻结',
                value: 2
              }]
            }, {
              label: '渠道ID',
              prop: 'tChannelkey'
            }, {
              label: '注册时间',
              prop: 'tRegtime',
              type: 'datetime',
              format: 'yyyy-MM-dd HH:mm:ss',
              valueFormat: 'timestamp',
            }, {
              label: '注册终端',
              prop: 'tRegterminaltype',
              type:'select',
              dicData: [{
                label: 'PC',
                value: 1
              }, {
                label: 'MOBILE',
                value: 2
              }, {
                label: 'H5',
                value: 3
              }]
            },{
              label: '注册IP',
              prop: 'tRegip'
            }, {
              label: '注册设备',
              span: 12,
              prop: 'tRegdevice',
            }, {
              label: '最后登入时间',
              prop: 'tLastlogintime',
              type: 'datetime',
              format: 'yyyy-MM-dd HH:mm:ss',
              valueFormat: 'timestamp',
            }, {
              label: '最后登出时间',
              prop: 'tLastlogouttime',
              type: 'datetime',
              format: 'yyyy-MM-dd HH:mm:ss',
              valueFormat: 'timestamp',
            }
            ]
        },
        {
          label: '账户信息',
          prop: 'account',
          icon: 'el-icon-view',
          column: [{
            label: '用户性别',
            prop: 'tSex',
            type:'select',
            dicData:
              [{
                label: '保密',
                value: '0'
              },{
                label: '男',
                value: 1
              },{
                label: '女',
                value: 2
              }]
          }, {
            label: 'VIP等级',
            prop: 'tViplevel',
            type:'select',
            dicData:
              [{
                label: '普通会员',
                value: 0
              },{
                label: 'VIP1',
                value: 1
              }, {
                label: 'VIP2',
                value: 2
              }, {
                label: 'VIP3',
                value: 3
              }, {
                label: 'VIP4',
                value: 4
              }, {
                label: 'VIP5',
                value:5
              }, {
                label: 'VIP6',
                value:6
              }, {
                label: 'VIP7',
                value:7
              }, {
                label: 'VIP8',
                value:8
              }, {
                label: 'VIP9',
                value:9
              }, {
                label: 'VIP10',
                value:10
              }]
          }, {
            label: '金币数',
            prop: 'tGoldcoin'
          }, {
            label: '银行金币数',
            prop: 'tBankgoldcoin'
          }, {
            label: '昵称修改',
            prop: 'tIsmodifiednick',
            type:'select',
            dicData:
              [{
                label: '未修改',
                value: 0
              },{
                label: '已修改',
                value: 1
              }]
          }, {
            label: '总计游戏时长',
            prop: 'tTotalgametime'
          }, {
            label: '今日游戏时长',
            prop: 'tTodayGameTime'
          }, {
            label: '累计充值金额',
            prop: 'tRechargemoney'
          },
          {
            label: '昨日下注积分',
            prop: 'tLastgamebetscore'
          },
          {
            label: '今日下注积分',
            prop: 'tTodaygamebetscore'
          }]
        }
    ]
}
