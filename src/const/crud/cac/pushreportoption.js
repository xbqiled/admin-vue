export default {
    option: [{
            label: '推送信息',
            prop: 'account',
            icon: 'el-icon-edit-outline',
            column: [{
                label: '消息编码',
                span: 24,
                prop: 'msg_id'
            }, {
                label: 'Android送达',
                span: 24,
                prop: 'android_received',
            }, {
              label: 'Ios推送到APNs',
              span: 24,
              prop: 'ios_apns_sent'
            }, {
              label: 'Ios通知送达到设备',
              span: 24,
              prop: 'ios_apns_received'
            }, {
              label: '自定义消息送达数',
              span: 24,
              prop: 'ios_msg_received'
            }, {
              label: 'Winphone通知送达',
              span: 24,
              prop: 'wp_mpns_sent'
            }]
        }
    ]
}
