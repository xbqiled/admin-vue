export const channelTableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menu: false,
  refreshBtn: false,
  filterBtn: false,
  dic: [],
  column: [
	  {
      label: '渠道ID',
      prop: 'channelId'
    },
    {
      label: '渠道名称',
      prop: 'channelName'
    },
    {
      label: '渠道图标',
      prop: 'iconUrl',
      type:'upload',
      imgWidth: 50,
      imgHeight: 50,
      listType:'picture-img'
    }
  ]
}
