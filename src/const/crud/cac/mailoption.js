export default {
    option: [{
            label: '邮件信息',
            prop: 'mail',
            icon: 'el-icon-edit-outline',
            column: [{
                label: '用户ID',
                prop: 'taccountid'
            }, {
                label: '用戶名',
                prop: 'taccountname',
            }, {
                label: '用户昵称',
                prop: 'tnickname'
            },{
              label: '邮件标题',
              prop: 'ttitle'
            }, {
              label: '发送人',
              prop: 'tsender'
            }, {
              label: '状态',
              prop: 'tstate',
              align: 'center',
              type: 'select',
              dicData:
                [{
                  label: '未读',
                  value: 1
                }, {
                  label: '已读',
                  value: 2
                }, {
                  label: '领取',
                  value: 3
                }]
            },  {
              label: '发送日期',
              prop: 'tsenddate',
              align: 'center',
              type: 'datetime',
              format: 'yyyy-MM-dd HH:mm:ss',
              valueFormat: 'timestamp',
            },
              {
                label: '接收日期',
                prop: 'trecvdate',
                align: 'center',
                type: 'datetime',
                format: 'yyyy-MM-dd HH:mm:ss',
                valueFormat: 'timestamp',
              }, {
              label: '邮件内容',
              span: 24,
              prop: 'content',
            }]
        }
    ]
}
