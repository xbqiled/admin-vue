export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
	  {
      label: 'ID',
      prop: 'userId',
      hide: true,
      addVisdiplay: false,
      editDisabled: true,
    },
	  {
      label: '用户名',
      prop: 'username',
      rules: [{
        required: true,
        message: '用户名不能为空',
        trigger: 'blur'
      }],
    },
    {
      label: '账号类型',
      prop: 'type',
      type: 'select',
      rules: [{
        required: true,
        message: '请选择账号类型',
        trigger: 'blur'
      }],
      dicData:
        [{
          label: '管理账号',
          value: 0
        }, {
          label: '渠道账号',
          value: 1
        }]
    },
	  {
      label: '邮箱',
      prop: 'email',
      rules: [{
        required: true,
        message: '请输入邮箱信息',
        trigger: 'blur'
      }],
    },
	  {
      label: '手机号',
      prop: 'mobile',
      rules: [{
        required: true,
        message: '请手机号信息',
        trigger: 'blur'
      }],
    },
	  {
      label: '状态',
      prop: 'status',
      type: 'select',
      rules: [{
        required: true,
        message: '请选择状态信息',
        trigger: 'blur'
      }],
      dicData:
        [{
          label: '禁用',
          value: '0'
        }, {
          label: '正常',
          value: '1'
        }]
    },
	  {
      label: '创建时间',
      prop: 'createTime'
    },
	  {
      label: '渠道ID',
      prop: 'channelId',
      rules: [{
        required: true,
        message: '请输入渠道编码',
        trigger: 'blur'
      }],
    },
  ]
}
