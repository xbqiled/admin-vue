export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
	  {
      label: 'ID',
      prop: 'id',
      hide: true,
      align: 'center',
      labelWidth: 100,
      editDisabled: true,
      addVisdiplay: false
    },
	  {
      label: '渠道ID',
      prop: 'channelId',
      labelWidth: 100,
      search:true,
      width: 100,
      align: 'center',
      rules: [{
        required: true,
        message: '渠道编码不能为空',
        trigger: 'blur'
      }],
    },
	  {
      label: 'android地址',
      prop: 'androidUrl',
      labelWidth: 100,
      width: 200,
      align: 'center',
      rules: [{
        required: true,
        message: 'ANDROID URL不能为空',
        trigger: 'blur'
      }],
    },
	  {
      label: 'ios地址',
      prop: 'iosUrl',
      labelWidth: 100,
      width: 200,
      align: 'center',
      rules: [{
        required: true,
        message: 'IOS URL不能为空',
        trigger: 'blur'
      }],
    },
	  {
      label: 'android版号',
      prop: 'androidVersion',
      labelWidth: 100,
      width: 120,
      rules: [{
        required: true,
        message: 'ANDROID 版本号不能为空',
        trigger: 'blur'
      }],
    },
	  {
      label: 'ios版号',
      prop: 'iosVersion',
      labelWidth: 100,
      width: 80,
      rules: [{
        required: true,
        message: 'IOS 版本号不能为空',
        trigger: 'blur'
      }],
    },
	  {
      label: '安卓appkey',
      prop: 'androidAppKey',
      width: 120,
      labelWidth: 100,
      rules: [{
        required: true,
        message: 'ANDROID APPKEY不能为空',
        trigger: 'blur'
      }],
    },
	  {
      label: '苹果appkey',
      prop: 'iosAppKey',
      width: 120,
      labelWidth: 100,
      rules: [{
        required: true,
        message: 'ANDROID APPKEY不能为空',
        trigger: 'blur'
      }],
    },
    {
      label: '安卓pkgId',
      prop: 'androidPkgid',
      labelWidth: 100,
      width: 120,
      rules: [{
        required: true,
        message: 'ANDROID PKGID不能为空',
        trigger: 'blur'
      }],
    },
    {
      label: '苹果pkgId',
      prop: 'iosPkgid',
      labelWidth: 100,
      width: 120,
      rules: [{
        required: true,
        message: 'IOS PKGID不能为空',
        trigger: 'blur'
      }],
    },
	  {
      label: 'scheme名称  ',
      prop: 'scheme',
      labelWidth: 100,
      width: 150,
      rules: [{
        required: true,
        message: 'scheme名称不能为空',
        trigger: 'blur'
      }],
    },
	  {
      label: 'host名称',
      prop: 'host',
      labelWidth: 100,
      width: 150,
      rules: [{
        required: true,
        message: 'scheme名称不能为空',
        trigger: 'blur'
      }]
    },
    {
      label: 'pathPrefix',
      prop: 'pathprefix',
      width: 150,
      labelWidth: 100,
      rules: [{
        required: true,
        message: 'pathPrefix不能为空',
        trigger: 'blur'
      }]
    },
	  {
      label: '是否激活',
      prop: 'flagActive',
      align: 'center',
      labelWidth: 100,
      width: 120,
      type: 'select',
      rules: [{
        required: true,
        message: '请选择是否激活',
        trigger: 'blur'
      }],
      dicData: [{
        label: '失效',
        value: '0'
      }, {
        label: '激活',
        value: '1'
      }]
    },
	  {
      label: '创建时间',
      prop: 'createTime',
      width: 220,
      labelWidth: 100,
      align: 'center',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm',
      valueFormat: 'timestamp',
      editDisabled: true,
      addVisdiplay: false
    },
	  {
      label: '创建人',
      prop: 'createBy',
      width: 150,
      labelWidth: 100,
      editDisabled: true,
      addVisdiplay: false
    },
	  {
      label: '修改时间',
      prop: 'modifyTime',
      align: 'center',
      hide:true,
      width: 220,
      labelWidth: 100,
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm',
      valueFormat: 'yyyy-MM-dd HH:mm:ss',
      addVisdiplay: false,
      editDisabled: true,
    },
	  {
      label: '修改人',
      prop: 'modifyBy',
      hide:true,
      labelWidth: 100,
      width: 150,
      editDisabled: true,
      addVisdiplay: false
    },
  ]
}
