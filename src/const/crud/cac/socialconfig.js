export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
	  {
      label: '主键',
      prop: 'id',
      hide: true,
      width: 100,
      align: 'center',
      editDisabled: true,
      addVisdiplay: false
    },
	  {
      label: '类型',
      prop: 'type',
      type: 'select',
      align: 'center',
      width: 120,
      rules: [{
        required: true,
        message: '类型不能为空',
        trigger: 'blur'
      }],
      dicData:
        [{
          label: '微信',
          value: 'wechat'
        }, {
          label: '微博',
          value: 'weibo'
        }]
    },
	  {
      label: 'APPID',
      prop: 'appId',
      width: 250,
      rules: [{
        required: true,
        message: 'APPID不能为空',
        trigger: 'blur'
      }],
    },
	  {
      label: 'SECRET',
      prop: 'appSecret',
      width: 300,
      rules: [{
        required: true,
        message: 'APPSECRET不能为空',
        trigger: 'blur'
      }],
    },
	  {
      label: '渠道编码',
      prop: 'channelId',
      width: 250,
      search:true,
      rules: [{
        required: true,
        message: '渠道编码不能为空',
        trigger: 'blur'
      }],
    },
	  {
      label: 'BUNDLEID',
      prop: 'bundleId',
      width: 300,
      rules: [{
        required: true,
        message: 'bundleId不能为空',
        trigger: 'blur'
      }],
    },
	  {
      label: '签名',
      prop: 'androidSign',
      width: 300,
      rules: [{
        required: true,
        message: '签名不能为空',
        trigger: 'blur'
      }],
    },
	  {
      label: '包名',
      prop: 'packageName',
      width: 250,
      rules: [{
        required: true,
        message: '包名不能为空',
        trigger: 'blur'
      }],
    },
	  {
      label: '状态',
      prop: 'status',
      align: 'center',
      width: 120,
      type: 'select',
      rules: [{
        required: true,
        message: '状态不能为空',
        trigger: 'blur'
      }],
      dicData:
        [{
          label: '失效',
          value: 0
        }, {
          label: '正常',
          value: 1
        }]
    },
	  {
      label: '创建人',
      prop: 'createBy',
      width: 150,
      editDisabled: true,
      addVisdiplay: false
    },
	  {
      label: '创建时间',
      prop: 'createTime',
      align: 'center',
      width: 220,
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm',
      valueFormat: 'yyyy-MM-dd HH:mm:ss',
      addVisdiplay: false,
      editDisabled: true,
    },
  ]
}
