export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
	  {
      label: 'ID',
      prop: 'id',
      hide: true,
      editDisabled: true,
      addVisdiplay: false
    },
	  {
      label: '客服名称',
      prop: 'csName',
      rules: [{
        required: true,
        message: '客服名称不能为空',
        trigger: 'blur'
      }],
    },
	  {
      label: '渠道编码',
      prop: 'channelId',
      search:true,
      rules: [{
        required: true,
        message: '渠道不能为空',
        trigger: 'blur'
      }],
    },
    {
      label: '联系通道',
      prop: 'type',
      type: 'select',
      dicData: [{
        label: '微信',
        value: 1
      }, {
        label: 'QQ',
        value: 2
      }, {
        label: '第三方',
        value: 3
      }, {
        label: '自有',
        value: 4
      }],
      rules: [{
        required: true,
        message: '联系通道不能为空',
        trigger: 'blur'
      }],
    },
	  {
      label: '客服类型',
      prop: 'connetType',
      type: 'select',
      dicData: [{
        label: '推广客服',
        value: 1
      }, {
        label: '游戏客服',
        value: 2
      }],
      rules: [{
        required: true,
        message: '客服类型不能为空',
        trigger: 'blur'
      }],
    },
	  {
      label: '账号URL',
      prop: 'accountOrUrl',
      rules: [{
        required: true,
        message: '账号URL不能为空',
        trigger: 'blur'
      }],
    },
	  {
      label: '状态',
      prop: 'status',
      type: 'select',
      rules: [{
        required: true,
        message: '状态不能为空',
        trigger: 'blur'
      }],
      dicData: [{
        label: '正常',
        value: 1
      }, {
        label: '失效',
        value: 0
      }],
    },
	  {
      label: '创建时间',
      prop: 'createTime',
      align: 'center',
      type: 'datetime',
      editDisabled: true,
      format: 'yyyy-MM-dd HH:mm',
      valueFormat: 'yyyy-MM-dd hh:mm:ss',
      addVisdiplay: false
    },
	  {
      label: '创建人',
      prop: 'createBy',
      align: 'center',
      editDisabled: true,
      addVisdiplay: false
    },
  ]
}
