export const tableOption = {
  border: true,
  index: false,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menuWidth: 120,
  dic: [],
  column: [
	  {
      label: '消息id',
      prop: 'msgId',
      width: 180
    },
	  {
      label: '渠道编码',
      prop: 'channelId',
      width: 150
    },
	  {
      label: '平台',
      prop: 'platform',
      type: 'select',
      width: 100,
      dicData:
        [{
          value: 'all',
          label: '全部平台'
        }, {
          value: 'ios',
          label: 'ios'
        }, {
          value: 'android',
          label: 'android'
        }, {
          value: 'winphone',
          label: 'winphone'
        }]
    },
    {
      label: '发送系统',
      prop: 'sendSys',
      width: 150,
      type: 'select',
      dicData:
        [{
          value: 0,
          label: '总控'
        }, {
          value: 1,
          label: '渠道'
        }]
    },
    {
      label: '通道名称',
      prop: 'platformName',
      width: 150
    },
    {
      label: '配置名称',
      prop: 'configName',
      width: 150
    },
	  {
      label: '标题',
      prop: 'title',
      width: 180
    },
	  {
      label: '内容体',
      prop: 'content',
      width: 220
    },
	  {
      label: '推送时间',
      prop: 'pushTimeStr',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm:ss',
      valueFormat: 'datetime'
    },
	  {
      label: '推送操作人',
      prop: 'pushBy'
    },
  ]
}
