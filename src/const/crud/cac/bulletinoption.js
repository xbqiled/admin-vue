export default {
  option: [{
    label: '公告信息',
    prop: 'mail',
    icon: 'el-icon-edit-outline',
    column: [{
      label: '公告标题',
      span: 24,
      prop: 'ttitle',
    }, {
      label: '公告内容',
      span: 24,
      prop: 'content',
    },{
      label:'图片地址',
      span: 24,
      prop:'thyperlink',
      type:'upload',
      imgWidth: 50,
      imgHeight: 50,
      listType:'picture-img'
    }]
  }
  ]
}
