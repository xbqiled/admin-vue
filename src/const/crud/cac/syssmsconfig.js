export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
	  {
      label: 'ID',
      prop: 'id',
      addVisdiplay: false,
      editDisabled: true,
      hide: true
    },
	  {
      label: '名称',
      search:true,
      prop: 'name',
      rules: [{
        required: true,
        message: '请输入名称',
        trigger: 'blur'
      }]
    },
    {
      label: '类型',
      prop: 'type',
      type: 'select',
      rules: [{
        required: true,
        message: '请选择类型',
        trigger: 'blur'
      }],
      dicData: [{
        label: '阿里大鱼',
        value: 1
      }, {
        label: '云信互联',
        value: 2
      }, {
        label: '容联云',
        value: 3
      }]
    },
	  {
      label: '类名',
      prop: 'className',
      rules: [{
        required: true,
        message: '请输入类名',
        trigger: 'blur'
      }]
    },
	  {
      label: '是否激活',
      prop: 'isActivate',
      type: 'select',
      rules: [{
        required: true,
        message: '请选择是否激活',
        trigger: 'blur'
      }],
      dicData: [{
        label: '失效',
        value: '0'
      }, {
        label: '激活',
        value: '1'
      }]
    },
	  {
      label: '创建时间',
      prop: 'createTime',
      align: 'center',
      type: 'datetime',
      editDisabled: true,
      format: 'yyyy-MM-dd HH:mm',
      valueFormat: 'yyyy-MM-dd hh:mm:ss',
      addVisdiplay: false
    },
	  {
      label: '创建人',
      prop: 'createBy',
      align: 'center',
      editDisabled: true,
      addVisdiplay: false
    },
    {
      label: '描述',
      prop: 'description',
      type: 'textarea',
      rules: [{
        required: true,
        message: '请输入短信内容',
        trigger: 'blur'
      }]
    },
  ]
}
