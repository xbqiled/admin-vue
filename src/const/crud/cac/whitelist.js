const DIC = {
  state: [{
    label: '正常',
    value: 0
  },
   {
      label: '失效',
      value: 1
   }]
}

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
	  {
      label: '用户ID',
      prop: 'userId'
    },
    {
      label: '用户名',
      prop: 'userName'
    },
    {
      label: '用户昵称',
      prop: 'nickName'
    },
    {
      label: '归属渠道',
      prop: 'channelNO'
    },
	  {
      label: '状态',
      prop: 'state',
      align: 'center',
      type: 'select',
      dicData: DIC.state
    },
  ]
}
