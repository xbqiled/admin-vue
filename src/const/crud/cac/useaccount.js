const DIC = {
  state: [{
    label: '正常',
    value: '0'
  },
  {
    label: '临时冻结',
    value: '1'
  },
  {
    label: '永久冻结',
    value: '2'
  }],
  type: [{
    label: '非正式',
    value: '0'
  },
    {
      label: '正式账号',
      value: '1'
    }]
}

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  menuWidth: 120,
  menuBtn: true,
  align: 'center',
  addBtn: false,
  editBtn: false,
  delBtn: false,
  viewBtn: false,
  menuType: 'menu',
  column: [
    {
      label: '用户ID',
      search:true,
      prop: 'tAccountid',
      width: 100,
      align: 'center',
      addVisdiplay: false
    },
	  {
      label: '用户名',
      search:true,
      width: 120,
      prop: 'tAccountname',
      align: 'center',
      rules: [{
        required: true,
        message: '用户名不能为空',
        trigger: 'blur'
      }],
    },
	  {
      label: '手机号',
      width: 120,
      prop: 'tPhonenumber',
      align: 'center',
      rules: [{
        required: true,
        message: '手机号不能为空',
        trigger: 'blur'
      }],
    },
	  {
      label: '昵称',
      width: 120,
      search:true,
      prop: 'tNickname',
      align: 'center',
      rules: [{
        required: true,
        message: '昵称不能为空',
        trigger: 'blur'
      }],
    },
	  {
      label: '账号类型',
      width: 80,
      prop: 'tBisregularaccount',
      align: 'center',
      type: 'select',
      rules: [{
        required: true,
        message: '账号类型不能为空',
        trigger: 'blur'
      }],
      dicData:
        [{
          label: '非正式',
          value: 0
        }, {
          label: '正式账号',
          value: 1
        }]
    },
	  {
      label: '状态',
      width: 100,
      prop: 'tAccountstate',
      align: 'center',
      type: 'select',
      rules: [{
        required: true,
        message: '请选择状态',
        trigger: 'blur'
      }],
      dicData: [{
        label: '正常',
        value: 0
      }, {
        label: '临时冻结',
        value: 1
      }, {
        label: '永久冻结',
        value: 2
      }]
    },
	  {
      label: '渠道ID',
      width: 120,
      prop: 'tChannelkey',
      align: 'center',
      search:true,
      editDisabled: true
    },
	  {
      label: '注册时间',
      width: 150,
      prop: 'tRegtime',
      align: 'center',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm',
      valueFormat: 'timestamp',
      editDisabled: true,
      addVisdiplay: false
    },
    {
      label: '最后登入',
      width: 150,
      prop: 'tLastlogintime',
      align: 'center',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm',
      valueFormat: 'timestamp',
      editDisabled: true,
      addVisdiplay: false
    },
    {
      label: '最后登出',
      width: 150,
      prop: 'tLastlogouttime',
      align: 'center',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm',
      valueFormat: 'timestamp',
      editDisabled: true,
      addVisdiplay: false
    },
    {
      label: '用户性别',
      width:  80,
      prop: 'tSex',
      type:'select',
      dicData:
        [{
          label: '保密',
          value: 0
        },{
          label: '男',
          value: 1
        },{
          label: '女',
          value: 2
        }]
    },
    {
      label: 'vip等级',
      width:  80,
      prop: 'tViplevel',
      type:'select',
      dicData:
        [{
          label: '普通会员',
          value: 0
        },{
          label: 'VIP1',
          value: 1
        }, {
          label: 'VIP2',
          value: 2
        }, {
          label: 'VIP3',
          value: 3
        }, {
          label: 'VIP4',
          value: 4
        }, {
          label: 'VIP5',
          value:5
        }, {
          label: 'VIP6',
          value:6
        }, {
          label: 'VIP7',
          value:7
        }, {
          label: 'VIP8',
          value:8
        }, {
          label: 'VIP9',
          value:9
        }, {
          label: 'VIP10',
          value:10
        }],
      editDisabled: true
    },
    {
      label: '金币数',
      width:  120,
      prop: 'tGoldcoin',
      editDisabled: true
    },
    {
      label: '银行金币',
      width:  120,
      prop: 'tBankgoldcoin',
      editDisabled: true
    },
    {
      label: '总计时长',
      width:  120,
      prop: 'tTotalgametime',
      editDisabled: true
    },
    {
      label: '今日时长',
      width:  120,
      prop: 'tTodaygametime',
      editDisabled: true
    },
    {
      label: '刷新时间',
      width:  200,
      prop: 'tTodaygametimerefreshtime',
      editDisabled: true
    },
    {
      label: '赢取金币',
      width:  120,
      prop: 'tTotalcoinget',
      editDisabled: true
    },
    {
      label: '今日金币',
      width:  120,
      prop: 'tTodaycoinget',
      editDisabled: true
    },
    {
      label: '金币刷新时间',
      width:  200,
      prop: 'tTodaycoinrefreshtime',
      editDisabled: true
    },
    {
      label: '累计充值',
      width:  120,
      prop: 'tRechargemoney',
      editDisabled: true
    },
    {
      label: '昨日下注积分',
      width:  120,
      prop: 'lastGameBetScore',
      editDisabled: true
    },
    {
      label: '今日下注积分',
      width:  120,
      prop: 'todayGameBetScore',
      editDisabled: true
    }
  ]
}
