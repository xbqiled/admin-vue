export const tableOption = {
  border: true,
  index: false,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menuWidth: 1,
  dic: [],
  column: [
	  {
      label: '主键',
      prop: 'id',
      hide: true
    },
	  {
      label: '渠道编码',
      prop: 'channelId'
    },
	  {
      label: '渠道名称',
      prop: 'channelName'
    },
	  {
      label: '真人账户余额',
      prop: 'videoBalance'
    },
    {
      label: '操作累加总额',
      prop: 'sumIncrease'
    },
    {
      label: '操作累减总额',
      prop: 'sumReduce'
    },
	  {
      label: '修改时间',
      prop: 'modifyTime'
    },
	  {
      label: '修改人',
      prop: 'modifyBy'
    }
  ]
}
