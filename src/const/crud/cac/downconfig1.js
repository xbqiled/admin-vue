export const tableOption = {
  border: true,
  index: false,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  menuWidth: 100,
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
    {
      label: 'ID',
      prop: 'id',
      hide: true
    },
    {
      label: '渠道编码',
      prop: 'channelId'
    },
    {
      label: '图标URL',
      prop: 'logoUrl'
    },
    {
      label: '客服url',
      prop: 'csUrl'
    },
    {
      label: '背景url',
      prop: 'backgroundUrl'
    },
    {
      label: '提示信息',
      prop: 'csTips'
    },
    {
      label: '活动信息',
      prop: 'activityMsg'
    },
    {
      label: '修改时间',
      prop: 'modifyTime'
    },
    {
      label: '修改人',
      prop: 'modifyBy'
    },
    {
      label: '创建时间',
      prop: 'createTime'
    },
    {
      label: '创建人',
      prop: 'createBy'
    },
  ]
}
