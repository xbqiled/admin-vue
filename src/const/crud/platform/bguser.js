export const tableOption = {
  border: true,
  index: false,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menuWidth: 150,
  dic: [],
  column: [
	  {
      label: '主键',
      prop: 'id',
      hide: true
    },
	  {
      label: '用户ID',
      prop: 'userId'
    },
	  {
      label: '昵称',
      prop: 'nickName'
    },
	  {
      label: '渠道编码',
      prop: 'channelId'
    },
	  {
      label: '厅代码',
      prop: 'sn'
    },
	  {
      label: '玩家状态',
      prop: 'status',
      type: 'select',
      dicData: [{
        label: '启用',
        value: 1
      }, {
        label: '挂起',
        value: 2
      }, {
        label: '取消',
        value: 3
      }, {
        label: '停用',
        value: 4
      }, {
        label: '冻结',
        value: 5
      }]
    },
	  {
      label: 'bg用户id',
      prop: 'bgUserId'
    },
	  {
      label: 'bg昵称',
      prop: 'bgNickname'
    },
	  {
      label: 'bg代理id',
      prop: 'agentId'
    },
	  {
      label: '创建时间',
      prop: 'createTime'
    },
  ]
}
