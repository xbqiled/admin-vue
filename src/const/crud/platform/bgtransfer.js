export const tableOption = {
  border: true,
  index: false,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menuWidth: 1,
  dic: [],
  column: [
	  {
      label: '主键',
      prop: 'id',
      hide: true
    },
	  {
      label: '业务单据',
      prop: 'bizId'
    },
	  {
      label: '厅号码',
      prop: 'sn'
    },
	  {
      label: '用户ID',
      prop: 'userId'
    },
	  {
      label: '状态',
      prop: 'status',
      type: 'select',
      dicData: [{
        label: '成功',
        value: 1
      }, {
        label: '失败',
        value: 2
      }]
    },
    {
      label: '转账金额',
      prop: 'amount'
    },
	  {
      label: 'bg余额',
      prop: 'bgBalance'
    },
	  {
      label: '系统余额',
      prop: 'sysBalance'
    },
	  {
      label: '会计项目',
      prop: 'accountItem',
      dicData: [{
        label: '转入|系统->BG',
        value: 1
      }, {
        label: '转出|BG->系统',
        value: 2
      }]
    },
	  {
      label: '操作时间',
      prop: 'operateTime',
      type: 'datetime',
      format: 'yyyy-MM-dd hh:mm:ss',
      valueFormat: 'yyyy-MM-dd hh:mm:ss',
    },
  ]
}
