export const tableOption = {
  border: true,
  index: false,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menuWidth: 1,
  menuBtn: false,
  dic: [],
  column: [
	  {
      label: '订单ID',
      prop: 'orderId'
    },
	  {
      label: '厅代码',
      prop: 'sn'
    },
	  {
      label: '用户ID',
      prop: 'uid'
    },
	  {
      label: '登录ID',
      prop: 'loginId'
    },
	  {
      label: '游戏名称',
      prop: 'gameName'
    },
	  {
      label: '注单状态',
      prop: 'orderStatus',
      type: 'select',
      dicData:
        [{
          label: '注单不存在',
          value: 0
        }, {
          label: '未结算',
          value: 1
        }, {
          label: '结算赢',
          value: 2
        }, {
          label: '结算和',
          value: 3
        }, {
          label: '结算输',
          value: 4
        }, {
          label: '取消',
          value: 5
        }, {
          label: '过期',
          value: 6
         } , {
          label: '系统取消',
          value: 7
        }]
    },
	  {
      label: '下注额',
      prop: 'bamount',
    },
	  {
      label: '结算额',
      prop: 'aamount'
    },
	  {
      label: '来源',
      prop: 'orderFrom',
      type: 'select',
      dicData:
        [{
          label: '未知终端',
          value: 0
        }, {
          label: 'PC版',
          value: 1
        }, {
          label: '安卓App',
          value: 2
        }, {
          label: '苹果App',
          value: 3
        }, {
          label: '平板电脑',
          value: 4
        } , {
          label: 'HTML-5',
          value: 5
        }]
    },
	  {
      label: '下注时间',
      prop: 'orderTime',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm:ss',
      valueFormat: 'yyyy-MM-dd hh:mm:ss',
    },
	  {
      label: '来源IP',
      prop: 'fromIp'
    },
	  {
      label: '下注期数',
      prop: 'issueId'
    },
	  {
      label: '玩法ID',
      prop: 'playId'
    },
	  {
      label: '玩法名称',
      prop: 'playName'
    },
	  {
      label: '玩法名称',
      prop: 'playNameEn'
    },
	  {
      label: '打码量',
      prop: 'validBet'
    },
	  {
      label: '派彩',
      prop: 'payment'
    },
  ]
}
