export const tableOption = {
  border: true,
  index: false,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menuWidth: 100,
  dic: [],
  column: [
	  {
      label: '主键',
      prop: 'id',
      hide: true
    },
	  {
      label: '渠道编码',
      prop: 'channelId'
    },
	  {
      label: '进入余额',
      prop: 'enterBalance'
    },
	  {
      label: '创建时间',
      prop: 'createTime',
      align: 'center',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm',
      valueFormat: 'yyyy-MM-dd HH:mm:ss',
      addVisdiplay: false,
      editDisabled: true,
    },
	  {
      label: '创建人',
      prop: 'createBy',
      addVisdiplay: false,
      editDisabled: true
    },
	  {
      label: '修改时间',
      prop: 'modifyTime',
      align: 'center',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm',
      valueFormat: 'yyyy-MM-dd HH:mm:ss',
      addVisdiplay: false,
      editDisabled: true,
    },
	  {
      label: '修改人',
      prop: 'modifyBy',
      addVisdiplay: false,
      editDisabled: true
    },
  ]
}
