export const tableOption = {
  border: true,
  index: false,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menuWidth: 1,
  menuBtn: false,
  dic: [],
  column: [
    {
      label: '局号',
      prop: 'serialNo'
    },
	  {
      label: '荷官ID',
      prop: 'dealerId'
    },
    {
      label: '游戏荷官 ',
      prop: 'dealer',
    },
    {
      label: '游戏ID',
      prop: 'gameType'
    },
	  {
      label: '游戏名称',
      prop: 'gameName'
    },
	  {
      label: '桌台ID',
      prop: 'tableId'
    },
	  {
      label: '游戏状态',
      prop: 'status',
      type: 'select',
      dicData:
        [{
          label: '未结算',
          value: 0
        }, {
          label: '正常结算',
          value: 1
        }, {
          label: '结算错误',
          value: 2
        }, {
          label: '游戏因故障中断',
          value: 3
        }, {
          label: '管理员中断游戏',
          value: 8
        } ]
    },
    {
      label: '开局时间',
      prop: 'openTime'
    },
    {
      label: '结算时间',
      prop: 'calcTime'
    }
  ]
}
