export const tableOption = {
  border: true,
  index: false,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  menuWidth: 150,
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
	  {
      label: '主键',
      prop: 'id',
      hide: true,
      width: 100,
      align: 'center',
      editDisabled: true,
      addVisdiplay: false
    },
	  {
      label: '平台编码',
      align: 'center',
      prop: 'platformCode',
      rules: [{
        required: true,
        message: '请输入平台编码',
        trigger: 'blur'
      }]
    },
	  {
      label: '平台名称',
      prop: 'platformName',
      align: 'center',
      rules: [{
        required: true,
        message: '请输入平台名称',
        trigger: 'blur'
      }]
    },
	  {
      label: '平台类型',
      prop: 'platformType',
      type: 'select',
      dicData: [ {
        label: 'BG',
        value: 1
      }, {
        label: 'AG',
        value: 2
      }],
      rules: [{
        required: true,
        message: '请选择平台类型',
        trigger: 'blur'
      }]
    },
	  {
      label: '平台配置',
      prop: 'platformConfig',
      hide: true,
      align: 'center',
      type: 'textarea',
      minRows: 3,
      span: 24,
      editDisabled: true,
      addVisdiplay: false
    },
	  {
      label: '状态',
      prop: 'status',
      align: 'center',
      type: 'select',
      dicData: [{
        label: '正常',
        value: 0
      }, {
        label: '维护',
        value: 1
      }, {
        label: '失效',
        value: 2
      }],
      rules: [{
        required: true,
        message: '请选择状态',
        trigger: 'blur'
      }]
    },
	  {
      label: '创建时间',
      prop: 'createTime',
      align: 'center',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm',
      valueFormat: 'yyyy-MM-dd hh:mm:ss',
      editDisabled: true,
      addVisdiplay: false
    },
	  {
      label: '创建人',
      prop: 'createBy',
      align: 'center',
      editDisabled: true,
      addVisdiplay: false
    },
	  {
      label: '修改时间',
      prop: 'modifyTime',
      align: 'center',
      hide: true,
      editDisabled: true,
      addVisdiplay: false
    },
	  {
      label: '修改人',
      prop: 'modifyBy',
      align: 'center',
      hide: true,
      editDisabled: true,
      addVisdiplay: false
    },
	  {
      label: '备注',
      align: 'center',
      prop: 'note',
      type: 'textarea',
      minRows: 3,
      span: 24
    },
  ]
}
