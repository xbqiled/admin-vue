export default {
    option: [{
            label: 'bg用户信息',
            prop: 'mail',
            icon: 'el-icon-edit-outline',
            column: [{
                label: '用户ID',
                prop: 'userId'
            }, {
                label: '登录ID',
                prop: 'loginId',
            }, {
                label: '用户昵称',
                prop: 'nickname'
            }, {
                label: '厅代码',
                prop: 'sn'
            }, {
                label: '登录次数',
                prop: 'loginCount'
            }, {
                label: '状态',
                prop: 'userStatus',
                align: 'center',
                type: 'select',
                dicData: [{
                  label: '启用',
                  value: 1
                }, {
                  label: '挂起',
                  value: 2
                }, {
                  label: '取消',
                  value: 3
                }, {
                  label: '停用',
                  value: 4
                }, {
                  label: '冻结',
                  value: 5
                }]
            },  {
              label: '注册日期',
              prop: 'regTime',
              align: 'center',
              type: 'datetime',
              format: 'yyyy-MM-dd HH:mm:ss',
              valueFormat: 'timestamp',
            },
              {
                label: '注册ip',
                prop: 'regIp',
                align: 'center'
              }, {
                label: 'bg备注',
                span: 24,
                prop: 'memo',
            }]
        }
    ]
}
