export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
	  {
      label: '主键',
      prop: 'id',
      hide: true,
      editDisabled: true,
      addVisdiplay: false
    },
	  {
      label: '渠道ID',
      search: true,
      prop: 'channelId'
    },
	  {
      label: '域名地址',
      prop: 'domainUrl',
      search: true,
      rules: [{
        required: true,
        message: '请输入域名地址信息',
        trigger: 'blur'
      }]
    },
	  {
      label: '域名数量',
      prop: 'num',
      addVisdiplay: false
    },
    {
      label: '状态',
      type: 'select',
      prop: 'status',
      rules: [{
        required: true,
        message: '请输入状态信息',
        trigger: 'blur'
      }],
      dicData:
        [{
          label: '正常',
          value: 1
        }, {
          label: '失效',
          value: 2
        }, {
          label: '拉黑',
          value: 3
        }]
    },
    {
      label: '创建时间',
      prop: 'createTime',
      type: 'datetime',
      width: 200,
      format: 'yyyy-MM-dd HH:mm:ss',
      valueFormat: 'datetime',
      editDisabled: true,
      addVisdiplay: false
    },
  ]
}
