export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menuWidth: 150,
  dic: [],
  column: [
    {
      label: 'ID',
      labelWidth: 100,
      prop: 'id',
      hide: true,
      editDisabled: true,
      addVisdiplay: false
    },
    {
      label: '名称',
      prop: 'name',
      labelWidth: 100,
      search: true,
      rules: [{
        required: true,
        message: '请名称信息',
        trigger: 'blur'
      }]
    },
    {
      label: '类型',
      labelWidth: 100,
      width: 100,
      type: 'select',
      prop: 'type',
      dicData:
        [{
          label: '第三方',
          value: 1
        }, {
          label: '本系统',
          value: 2
        }],
      rules: [{
        required: true,
        message: '请输入类型信息',
        trigger: 'blur'
      }]
    },
    {
      label: '参数位置',
      prop: 'replacePostion',
      labelWidth: 100,
      type: 'select',
      dicData:
        [{
          label: 'URLPRAR',
          value: 0
        },{
          label: 'BODY',
          value: 2
        }],
      rules: [{
        required: true,
        message: '请输入参数位置信息',
        trigger: 'blur'
      }]
    },
    {
      label: '状态',
      prop: 'status',
      labelWidth: 100,
      type: 'select',
      dicData:
        [{
          label: '正常',
          value: 1
        }, {
          label: '失效',
          value: 2
        }],
      rules: [{
        required: true,
        message: '请输入状态信息',
        trigger: 'blur'
      }]
    },
    {
      label: '请求类型',
      prop: 'reqType',
      labelWidth: 100,
      type: 'select',
      dicData:
        [{
          label: 'POST',
          value: 0
        }, {
          label: 'GET',
          value: 2
        }],
      rules: [{
        required: true,
        message: '请输入请求类型信息',
        trigger: 'blur'
      }]
    },
	  {
      label: 'URL地址',
      labelWidth: 100,
      width: 300,
      span: 24,
      prop: 'url',
      rules: [{
        required: true,
        message: '请输入URL地址信息',
        trigger: 'blur'
      }]
    },
    {
      label: 'HeadJson',
      labelWidth: 100,
      type: 'textarea',
      span: 24,
      hide:true,
      prop: 'headJson'
    },
    {
      label: 'UrlPara',
      labelWidth: 100,
      type: 'textarea',
      span: 24,
      hide:true,
      prop: 'urlPara'
    },
    {
      label: 'BodyJson',
      labelWidth: 100,
      type: 'textarea',
      span: 24,
      hide:true,
      prop: 'bodyJson'
    },
	  {
      label: '请求次数',
      labelWidth: 100,
      width: 80,
      prop: 'reqCount',
      editDisabled: true,
      addVisdiplay: false
    },
	  {
      label: '创建人',
      labelWidth: 100,
      prop: 'createBy',
      hide: true,
      editDisabled: true,
      addVisdiplay: false
    },
	  {
      label: '创建时间',
      width: 180,
      prop: 'createTime',
      labelWidth: 100,
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm:ss',
      valueFormat: 'datetime',
      editDisabled: true,
      addVisdiplay: false
    }
  ]
}
