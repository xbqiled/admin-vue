export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  column: [
    {
      label: 'ID',
      prop: 'id',
      hide: true,
      editDisabled: true,
      addVisdiplay: false
    },
    {
      label: '关键值',
      prop: 'serialNum',
      hide: true,
      editDisabled: true,
      addVisdiplay: false
    },
    {
      label: '入口地址',
      prop: 'shortUrl',
      search: true,
      rules: [{
        required: true,
        message: '请入口地址信息',
        trigger: 'blur'
      }]
    },
    {
      label: '跳转地址',
      prop: 'jumpUrl',
      rules: [{
        required: true,
        message: '请跳转地址信息',
        trigger: 'blur'
      }]
    },
    {
      label: '渠道编码',
      prop: 'channelId',
      search: true,
      rules: [{
        required: true,
        message: '请渠道编码信息',
        trigger: 'blur'
      }]
    },
    {
      label: '落地地址',
      prop: 'sourceUrl',
      rules: [{
        required: true,
        message: '请落地地址信息',
        trigger: 'blur'
      }]
    },
    {
      label: '创建时间',
      prop: 'createTime',
      type: 'datetime',
      width: 200,
      format: 'yyyy-MM-dd HH:mm:ss',
      valueFormat: 'datetime',
      editDisabled: true,
      addVisdiplay: false
    },
    {
      label: '状态',
      prop: 'status',
      search: true,
      width: 150,
      type: 'select',
      rules: [{
        required: true,
        message: '请输入创建时间信息',
        trigger: 'blur'
      }],
      dicData:
        [{
          label: '正常',
          value: 1
        }, {
          label: '失效',
          value: 2
        }, {
          label: '拉黑',
          value: 3
        }]
    },
  ]
}
