export default {
  option: [{
    label: '手牌信息',
    prop: 'cardRecordName',
    icon: 'el-icon-edit-outline',
    column: [{
      label: '手牌信息',
      span: 24,
      prop: 'cardRecordName'
    }
    ]
  },
    {
      label: '下注信息',
      prop: 'betDetailName',
      icon: 'el-icon-view',
      column: [{
        label: '下注信息',
        span: 24,
        prop: 'betDetailName'
      }]
    }
  ]
}
