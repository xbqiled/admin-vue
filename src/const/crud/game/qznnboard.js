export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  menuWidth: 120,
  column: [
    {
      label: '牌局ID',
      prop: 'RecordID'
    },
    {
      label: '本桌人数',
      prop: 'playerCnt'
    },
    {
      label: '庄家ID',
      prop: 'bankerId'
    },
    {
      label: '底注',
      prop: 'ante',
      width: 100
    },
    {
      label: '用户ID',
      prop: 'UserID',
      width: 100
    },
    {
      label: '角色',
      prop: 'role',
      width: 100,
      type: 'select',
      dicData:
        [{
          label: '闲家',
          value: 0
        }, {
          label: '庄家',
          value: 1
        }]
    },
    {
      label: '庄家抢分',
      prop: 'bankerScore',
      width: 120,
      type: 'select',
      dicData:
        [{
          label: '抢1',
          value: 1
        }, {
          label: '抢2',
          value: 2
        }, {
          label: '抢3',
          value: 3
        }, {
          label: '抢4',
          value: 4
        }]
    },
    {
      label: '之前金额(分)',
      prop: 'beforeCoin',
      width: 120
    },
    {
      label: '变化金额(分)',
      prop: 'changeCoin',
      width: 120
    },
    {
      label: '之后金额(分)',
      prop: 'afterCoin',
      width: 120
    },
    {
      label: '牌型倍数',
      prop: 'winMultiple',
      width: 80
    },
    {
      label: '输赢类型',
      prop: 'winType',
      width: 80,
      type: 'select',
      dicData:
        [{
          label: '庄家胜',
          value: 1
        }, {
          label: '闲家胜',
          value: 2
        }]
    },
    {
      label: '游戏名称',
      prop: 'gameKindName'
    },
    {
      label: '游戏房间',
      prop: 'phoneGameName'
    },
    {
      label: '开始时间',
      prop: 'StartTime'
    },
    {
      label: '结束时间',
      prop: 'EndTime'
    },
    {
      label: '渠道编码',
      prop: 'ChannelNO'
    }
  ]
}
