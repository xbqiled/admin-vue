export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  menuWidth: 120,
  column: [
    {
      label: '牌局ID',
      prop: 'RecordID'
    },
    {
      label: '用户ID',
      prop: 'UserID'
    },
    {
      label: '用户类型',
      prop: 'UserType',
      width: 100,
      hide: true,
      type: 'select',
      dicData:
        [{
          label: '下注玩家',
          value: 0
        }, {
          label: '庄家',
          value: 1
        }, {
          label: '未下注玩家',
          value: 2
        }]
    },
    {
      label: '下注总分',
      prop: 'TotalAmount'
    },
    {
      label: '玩家总得分',
      prop: 'TotalScore'
    },
    {
      label: '结算总分',
      prop: 'Gain'
    },
    {
      label: '游戏名称',
      prop: 'gameKindName'
    },
    {
      label: '游戏房间',
      prop: 'phoneGameName'
    },
    {
      label: '开始时间',
      prop: 'StartTime'
    },
    {
      label: '结束时间',
      prop: 'EndTime',
      width: 200
    },
    {
      label: '渠道编码',
      prop: 'ChannelNO',
      width: 120
    }
  ]
}
