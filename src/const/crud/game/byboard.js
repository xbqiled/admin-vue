export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  menuWidth: 120,
  column: [
	  {
      label: '牌局编码',
      prop: 'RecordID',
      width: 220
    },
	  {
      label: '用户ID',
      prop: 'UserID',
      width: 120
    },
    {
      label: '开炮数',
      prop: 'FireNumber',
      width: 100,
      hide: true
    },
	  {
      label: '开炮消耗分数',
      prop: 'ConsumptionAmount',
      width: 120,
      hide: true
    },
    {
      label: '抽水',
      prop: 'Rebate',
      width: 120,
      hide: true
    },
    {
      label: '吞炮金币数',
      prop: 'LoseGold',
      width: 120,
      hide: true
    },
    {
      label: '结算分数',
      prop: 'EndIntegral',
      width: 120
    },
    {
      label: '贵宾房间',
      prop: 'IsVip',
      width: 120,
      type: 'select',
      dicData:
        [{
          label: '否',
          value: 0
        }, {
          label: '是',
          value: 1
        }]
    },
    {
      label: '轮数',
      prop: 'Round',
      width: 120,
      hide: true
    },
    {
      label: '终端',
      prop: 'TerminalType',
      width: 120,
      type: 'select',
      dicData:
        [{
          label: 'PC端',
          value: 0
        }, {
          label: 'Android',
          value: 1
        }, {
          label: 'Ios',
          value: 2
        }, {
          label: 'H5',
          value: 3
        }]
    },
	  {
      label: '游戏名称',
      prop: 'gameKindName',
      width: 120
    },
	  {
      label: '游戏房间',
      prop: 'phoneGameName',
      width: 200
    },
	  {
      label: '开始时间',
      prop: 'StartTime',
      width: 180
    },
	  {
      label: '结束时间',
      prop: 'EndTime',
      width: 180
    },
    {
      label: '渠道编码',
      prop: 'ChannelNO',
      width: 120
    }
  ]
}
