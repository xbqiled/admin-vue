export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menu: false,
  dic: [],
  column: [
    {
      label: '牌局ID',
      prop: 'RecordID'
    },
	  {
      label: '用户ID',
      prop: 'UserID'
    },
	  {
      label: '开奖期数',
      prop: 'Periodical',
      width: 100
    },
	  {
      label: '牌面值',
      prop: 'cardRecordName',
      width: 80
    },
    {
      label: '下注详情',
      prop: 'betDetailName'
    },
	  {
      label: '下注总分',
      prop: 'TotalAmount',
      width: 120
    },
	  {
      label: '玩家总得分',
      prop: 'TotalScore',
      width: 120
    },
	  {
      label: '结算总分',
      prop: 'Gain',
      width: 120
    },
    {
      label: '结算总分',
      prop: 'Gain',
      width: 120
    },
	  {
      label: '强杀第几局',
      prop: 'killInningIndex',
      width: 120
    },
    {
      label: '本局强杀',
      prop: 'isCurInningKill',
      width: 80,
      type: 'select',
      dicData: [{
        label: '否',
        value: 0
      }, {
        label: '是',
        value: 1
      }]
    },
    {
      label: '本局真人龙虎实际总下注',
      prop: 'curActualTotalBet'
    },
    {
      label: '强杀真人龙虎最大总下注',
      prop: 'killMaxTotalBet'
    },
    {
      label: '游戏房间',
      prop: 'phoneGameName'
    },
    {
      label: '开始时间',
      prop: 'StartTime'
    },
    {
      label: '结束时间',
      prop: 'EndTime'
    },
    {
      label: '渠道编码',
      prop: 'ChannelNO',
      width: 110
    }
  ]
}
