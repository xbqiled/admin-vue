export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  menuWidth: 120,
  column: [
	  {
      label: '牌局ID',
      prop: 'RecordID',
      width: 300
    },
	  {
      label: '用户ID',
      prop: 'UserID',
      width: 120
    },
	  {
      label: '开奖期数',
      prop: 'Periodical',
      width: 120
    },
	  {
      label: '下注总分',
      prop: 'TotalAmount',
      width: 120
    },
	  {
      label: '玩家总得分',
      prop: 'TotalScore',
      width: 120
    },
	  {
      label: '结算总分',
      prop: 'Gain',
      width: 120
    },
    {
      label: '游戏名称',
      prop: 'gameKindName',
      width: 120
    },
    {
      label: '游戏房间',
      prop: 'phoneGameName',
      width: 120
    },
    {
      label: '开始时间',
      prop: 'StartTime',
      width: 180
    },
    {
      label: '结束时间',
      prop: 'EndTime',
      width: 180
    },
    {
      label: '渠道编码',
      prop: 'ChannelNO',
      width: 120
    }
  ]
}
