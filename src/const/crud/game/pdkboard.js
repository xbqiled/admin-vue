export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  menuWidth: 120,
  column: [
	  {
      label: '牌局ID',
      prop: 'RecordID',
      width: 200
    },
	  {
      label: '用户ID',
      prop: 'UserID',
      width: 120
    },

	  {
      label: '当局金额(分)',
      prop: 'TotalScore',
      width: 120
    },
	  {
      label: '椅子号',
      prop: 'ChairID',
      width: 100
    },
    {
      label: '出牌次数',
      prop: 'HandTotalCount',
      width: 100
    },
    {
      label: '托管出牌次数',
      prop: 'HosteTotalCount',
      width: 100
    },
    {
      label: '是否托管惩罚',
      prop: 'IsHostePunish',
      width: 100
    },
    {
      label: '第几轮',
      prop: 'SeveralRounds',
      width: 100
    },
    {
      label: '游戏名称',
      prop: 'gameKindName',
      width: 120
    },
    {
      label: '游戏房间',
      prop: 'phoneGameName',
      width: 120
    },
	  {
      label: '开始时间',
      prop: 'StartTime',
      width: 180
    },
	  {
      label: '结束时间',
      prop: 'EndTime',
      width: 180
    },
    {
      label: '渠道编码',
      prop: 'ChannelNO',
      width: 120
    }
  ]
}
