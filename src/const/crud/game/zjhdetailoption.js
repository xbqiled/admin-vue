export default {
  option: [{
    label: '牌面信息',
    prop: 'recordInfoName',
    icon: 'el-icon-view',
    column: [{
      label: '牌面信息',
      span: 24,
      prop: 'recordInfoName'
    }]
  },
    {
      label: '结算分数',
      prop: 'endIntegralName',
      icon: 'el-icon-edit-outline',
      column: [{
        label: '输赢积分',
        span: 24,
        prop: 'endIntegralName'
      }]
    }, {
      label: '玩家信息',
      prop: 'userInfoName',
      icon: 'el-icon-share',
      column: [{
        label: '玩家信息',
        span: 24,
        prop: 'userInfoName'
      }]
    }
  ]
}
