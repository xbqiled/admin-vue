export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  menuWidth: 120,
  column: [
    {
      label: '牌局ID',
      prop: 'RecordID',
      width: 200
    },
    {
      label: '用户ID',
      prop: 'UserID',
      width: 120
    },
    {
      label: '玩家昵称',
      prop: 'nickname',
      width: 100
    },
    {
      label: '开奖格子id',
      prop: 'gridId',
      width: 100
    },
    {
      label: '金鲨倍数',
      prop: 'goldShark',
      width: 100
    },
    {
      label: '银鲨倍数',
      prop: 'sliverShark',
      width: 100
    },
    {
      label: '玩家总盈利(分)',
      prop: 'totalProfit',
      width: 120
    },
    {
      label: '之前金额(分)',
      prop: 'beforeCoin',
      width: 120
    },
    {
      label: '金额变化(分)',
      prop: 'changeCoin',
      width: 120
    },
    {
      label: '之后金额(分)',
      prop: 'afterCoin',
      width: 120
    },
    {
      label: '税收(分)',
      prop: 'tax',
      width: 120
    },
    {
      label: '税收比例',
      prop: 'taxRate',
      width: 120
    },
    {
      label: '游戏房间',
      prop: 'phoneGameName',
      width: 120
    },
    {
      label: '游戏房间',
      prop: 'phoneGameName',
      width: 200
    },
    {
      label: '开始时间',
      prop: 'StartTime',
      width: 200
    },
    {
      label: '结束时间',
      prop: 'EndTime',
      width: 200
    },
    {
      label: '渠道编码',
      prop: 'ChannelNO',
      width: 120
    }
  ]
}
