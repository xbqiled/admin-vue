export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  menuWidth: 120,
  column: [
	  {
      label: '牌局ID',
      prop: 'RecordID',
      width: 200
    },
	  {
      label: '用户ID',
      prop: 'UserID'
    },
	  {
      label: '开奖期数',
      prop: 'Periodical'
    },
    {
      label: '当期下注总分',
      prop: 'TotalAmount',
      width: 120
    },
    {
      label: '玩家总得分',
      prop: 'TotalScore'
    },
    {
      label: '结算总分',
      prop: 'Gain'
    },
    {
      label: '游戏名称',
      prop: 'gameKindName'
    },
    {
      label: '游戏房间',
      prop: 'phoneGameName'
    },
    {
      label: '开始时间',
      prop: 'StartTime'
    },
    {
      label: '结束时间',
      prop: 'EndTime'
    },
    {
      label: '渠道编码',
      prop: 'ChannelNO',
      width: 150
    }
  ]
}
