export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  menuWidth: 120,
  column: [
    {
      label: '牌局ID',
      prop: 'RecordID',
      width: 200
    },
    {
      label: '用户ID',
      prop: 'UserID',
      width: 120
    },
    {
      label: '椅子号',
      prop: 'ChairID',
      width: 80
    },
    {
      label: '出牌总次数',
      prop: 'HandTotalCount',
      width: 100,
      hide: true
    },
    {
      label: '托管出牌总次数',
      prop: 'HosteTotalCount',
      width: 100,
      hide: true
    },
    {
      label: '是否托管惩罚',
      prop: 'IsHostePunish',
      width: 100,
      hide: true,
      type: 'select',
      dicData:
        [{
          label: '否',
          value: 0
        }, {
          label: '是',
          value: 1
        }]
    },
    {
      label: '玩家当局得分',
      prop: 'TotalScore'
    },
    {
      label: '第几轮',
      prop: 'SeveralRounds',
      width: 120,
      hide: true
    },
    {
      label: '游戏名称',
      prop: 'gameKindName',
      width: 130
    },
    {
      label: '游戏房间',
      prop: 'phoneGameName'
    },
    {
      label: '开始时间',
      prop: 'StartTime'
    },
    {
      label: '结束时间',
      prop: 'EndTime'
    },
    {
      label: '渠道编码',
      prop: 'ChannelNO'
    }
  ]
}
