export default {
  option: [{
    label: '牌局详情',
    prop: 'recordInfoName',
    icon: 'el-icon-view',
    column: [{
      label: '初始牌',
      span:24,
      prop: 'recordInfoName'
    }]
  },
    {
      label: '输赢积分',
      prop: 'loseOrWinIntegalName',
      icon: 'el-icon-edit-outline',
      column: [{
        label: '输赢积分',
        span:24,
        prop: 'loseOrWinIntegalName'
      }]
    }, {
      label: '结算分数',
      prop: 'endIntegralName',
      icon: 'el-icon-edit',
      column: [{
        label: '结算分数',
        span:24,
        prop: 'endIntegralName'
      }]
    }, {
      label: '玩家信息',
      prop: 'userInfoName',
      icon: 'el-icon-share',
      column: [{
        label: '玩家信息',
        span:24,
        prop: 'userInfoName'
      }]
    }
  ]
}
