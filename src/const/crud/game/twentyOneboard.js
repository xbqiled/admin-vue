export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  dic: [],
  menuWidth: 120,
  column: [
	  {
      label: '牌局ID',
      prop: 'RecordID',
      width: 200
    },
	  {
      label: '用户ID',
      prop: 'UserID',
      width: 120
    },
	  {
      label: '本桌人数',
      prop: 'PlayerCnt',
      width: 150
    },
	  {
      label: '下注总金额(分)',
      prop: 'TotalBet',
      width: 150
    },
    {
      label: '税收(分)',
      prop: 'Tax',
      width: 150
    },
    {
      label: '下注前金额',
      prop: 'BeforeCoin',
      width: 150
    },
    {
      label: '变化金额',
      prop: 'ChangeCoin',
      width: 150
    },
    {
      label: '下注后金额',
      prop: 'AfterCoin',
      width: 150
    },
    {
      label: '庄家牌型',
      prop: 'BankerDim',
      width: 150
    },
    {
      label: '庄家点数',
      prop: 'BankerPoint',
      width: 150
    },
    {
      label: '玩家昵称',
      prop: 'NickName',
      width: 150
    },
    {
      label: '游戏名称',
      prop: 'gameKindName',
      width: 120
    },
    {
      label: '游戏房间',
      prop: 'phoneGameName',
      width: 120
    },
	  {
      label: '开始时间',
      prop: 'StartTime',
      width: 180
    },
	  {
      label: '结束时间',
      prop: 'EndTime',
      width: 180
    },
    {
      label: '渠道编码',
      prop: 'ChannelNO',
      width: 120
    }
  ]
}
