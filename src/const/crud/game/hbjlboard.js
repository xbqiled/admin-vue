export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menu: false,
  dic: [],
  menuWidth: 120,
  column: [
	  {
      label: '牌局ID',
      prop: 'RecordID',
      width: 200
    },
	  {
      label: '用户ID',
      prop: 'UserID',
      width: 120
    },
	  {
      label: '开奖期数',
      prop: 'Periodical',
      width: 150
    },
    {
      label: '用户类型',
      prop: 'UserType',
      type: 'select',
      width: 100,
      dicData:
        [{
          label: '发红包者',
          value: 1
        }, {
          label: '抢红包者',
          value: 2
        }]
    },
    {
      label: '红包总金额',
      prop: 'RedEnvelopeAmount',
      width: 150
    },
    {
      label: '系统抽水',
      prop: 'Tax',
      width: 150
    },
    {
      label: '抢到金额',
      prop: 'GrabAmount',
      width: 120
    },
    {
      label: '下一轮发红包者',
      prop: 'IsNext',
      type: 'select',
      width: 120,
      dicData:
        [{
          label: '是',
          value: 1
        }, {
          label: '否',
          value: 0
        }]
    },
    {
      label: '游戏名称',
      prop: 'gameKindName',
      width: 120
    },
    {
      label: '游戏房间',
      prop: 'phoneGameName',
      width: 120
    },
    {
      label: '开始时间',
      prop: 'StartTime'
    },
    {
      label: '结束时间',
      prop: 'EndTime'
    },
    {
      label: '渠道编码',
      prop: 'ChannelNO',
      width: 150
    }
  ]
}
