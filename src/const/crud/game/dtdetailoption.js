export default {
  option: [{
    label: '牌面信息',
    prop: 'cardRecordName',
    icon: 'el-icon-view',
    column: [{
      label: '牌面信息',
      span:24,
      prop: 'cardRecordName'
    }]
  },
    {
      label: '下注结算',
      prop: 'betDetailName',
      icon: 'el-icon-edit-outline',
      column: [{
        label: '下注记录',
        span:24,
        prop: 'betDetailName'
      }]
    }
  ]
}
