export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn: false,
  delBtn: false,
  addBtn: false,
  menu: false,
  dic: [],
  menuWidth: 120,
  column: [
	  {
      label: '牌局ID',
      prop: 'RecordID',
      width: 200
    },
	  {
      label: '用户ID',
      prop: 'UserID'
    },
	  {
      label: '开奖期数',
      prop: 'Periodical'
    },
    {
      label: '名次',
      prop: 'betOrder',
      width: 120
    },
    {
      label: '获奖前分数',
      prop: 'beforeCoin'
    },
    {
      label: '奖金',
      prop: 'reward'
    },
    {
      label: '获奖后分数',
      prop: 'afterCoin'
    },
    {
      label: '游戏房间',
      prop: 'phoneGameName'
    },
    {
      label: '开始时间',
      prop: 'StartTime'
    },
    {
      label: '结束时间',
      prop: 'EndTime'
    },
    {
      label: '渠道编码',
      prop: 'ChannelNO',
      width: 150
    }
  ]
}
