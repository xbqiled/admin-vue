export default {
  option: [{
    label: '牌局详情',
    prop: 'recordInfoName',
    icon: 'el-icon-view',
    column: [{
      label: '初始牌',
      span:24,
      prop: 'recordInfoName'
    }]
  }, {
      label: '免费次数',
      prop: 'endIntegralName',
      icon: 'el-icon-edit',
      column: [{
        label: '免费次数',
        span:24,
        prop: 'endIntegralName'
      }]
    }
  ]
}
