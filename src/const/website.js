export default {
  title: 'ABC',
  logo: 'ABC',
  indexTitle: 'ABC棋牌管理系统',
  whiteList: ['/login', '/404', '/401', '/lock'], // 配置无权限可以访问的页面
  whiteTagList: ['/login', '/404', '/401', '/lock' ], // 配置不添加tags页面 （'/advanced-router/mutative-detail/*'——*为通配符）
  lockPage: '/lock',
  tokenTime: 6000,
  info: {
    title: 'ABC棋牌管理系统',
    list: [
      '全球首家百万人在线棋牌竞技中心， 专业的棋牌娱乐平台。',
      '本着做棋牌娱乐第一品牌的理念和信念。',
      '多年来始终专注于游戏项目的开发与运营，为了给众多玩家们营造一个和谐、健康的娱乐环境。'
    ]
  },
  // http的status默认放行不才用统一处理的,
  // 配置首页不可关闭
  isFirstPage: false,
  fistPage: {
    label: '首页',
    value: '/wel/index',
    params: {},
    query: {},
    group: [],
    close: false
  },
  // 配置菜单的属性
  menu: {
    props: {
      label: 'label',
      path: 'path',
      icon: 'icon',
      children: 'children'
    }
  }
}
