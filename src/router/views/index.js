import Layout from '@/page/index/'

export default [{
  path: '/wel',
  component: Layout,
  redirect: '/wel/index',
  children: [{
    path: 'index',
    name: '首页',
    component: () =>
      import ( /* webpackChunkName: "views" */ '@/page/wel')
  }]
}, {
  path: '/echar',
  component: Layout,
  redirect: '/echar/index',
  children: [{
    path: 'index',
    name: '图表',
    component: () => import ( /* webpackChunkName: "views" */ '@/page/echar')
  }]
}, {
  path: '/info',
  component: Layout,
  redirect: '/info/index',
  children: [{
    path: 'index',
    name: '个人信息',
    component: () =>
      import ( /* webpackChunkName: "page" */ '@/views/admin/user/info'),
  }]
}]
